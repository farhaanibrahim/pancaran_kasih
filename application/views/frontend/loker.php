<!DOCTYPE html>
<html lang="en">
<head>
  <title>LPK Pancaran Kasih-Loker</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/bootstrap.min.js"></script>

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/favicon.png" />
  <style>
  .navbar {
    background:#34792A;
    border-color: transparent;
    margin-bottom: 0;
    border-radius: 0;
    text-shadow: 1px 1px 1px #000
  }
  .navbar li a, .navbar {
    color: #93f765 !important;
    font-size: 11px
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #34792A !important;
    background-color: #93f765 !important;
    text-shadow: 1px 0.5px 0.5px #000
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  @media (min-width: 768px) {
    .navbar-nav.navbar-center {
      position: absolute;
      left: 50%;
      transform: translatex(-50%);
    }
  }

  footer {
    background: #fff; 
    color:#000;
    text-align: center; 
    padding-top: 20px;
  }
  .card {
    box-shadow: 0 4px 8px 0 rgba(52,121,42,0.5);
    transition: 0.3s;
    background-color: #93F765;
    padding-left: 20px;
    padding-top: 1px;
    padding-bottom: 10px;
    padding-right:15px;
  }
  .card:hover {
    box-shadow: 0 16px 32px 0 rgba(52,121,42,0.5);
  }

</style>
</head>
<body>
  <!-- awal nav -->
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="background: #93f765">
          <span style="background: #34792A" class="icon-bar"></span>
          <span style="background: #34792A" class="icon-bar"></span>
          <span style="background: #34792A" class="icon-bar"></span>                     
        </button>
        <a href="<?php echo base_url(); ?>" class="navbar-brand" style="color: #93f765; border-bottom: 3px solid #93f765">LPK Pancaran Kasih</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-center">
          <li><a href="<?php echo base_url(); ?>">HOMEPAGE</a></li>
          <li><a href="<?php echo base_url(); ?>gallery">GALLERY</a></li>
          <li class="active"><a href="<?php echo base_url(); ?>loker">LOKER</a></li>
          <li><a href="<?php echo base_url(); ?>pekerja">CARI PEKERJA</a></li>
          <li><a href="<?php echo base_url(); ?>artikel">ARTIKEL</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo base_url(); ?>kontak"><span class="glyphicon glyphicon-envelope"></span> KONTAK</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- akhir nav -->
  <div class="container col-md-6 col-md-offset-3" style=" margin-top:80px"> 
    <div><h2 style="color: #34792a;text-shadow: 1px 1px 0px #000" class="text-center">LOWONGAN PEKERJAAN</h2></div><br>
    <!-- mulai persayratan -->
    <div class="card">
      <h4>Persyaratan :</h4>
      <p>
        <ul>
          <li>Ijazah / Buku nikah asli</li>
          <li>Photocopy KTP</li>
          <li>Siap merantau / Siap ditempatkan dimana saja</li>
          <li>Siap tinggal dalam / di rumah majikan</li>
          <li>Siap mengikuti peraturan yayasan</li>
          <li>Serius ingin bekerja</li>
        </ul>
      </p>
    </div>   
    <!-- akhir persayratan -->

    <!-- mulai formnya -->
    <div style=" margin-top:10px">
      <div class="container col-xs-12 col-md-12">
        <div class="panel panel-info" style="border-color: #34792A;">
          <div class="panel-heading" style="background-color: #34792A;color: #93F765;text-shadow: 1px 0.5px 0.5px #000">
            <div class="panel-title text-center">Input Data Diri</div>
          </div>  
          <div class="panel-body">
            <form class="form-horizontal" method="post" action="<?php echo base_url('loker'); ?>" enctype="multipart/form-data">
                <?php if($this->session->flashdata('error_lampiran')): ?>
                <div class="alert alert-danger" role="alert">
                  <?php echo $this->session->flashdata('error_lampiran'); ?>
                </div>
                <?php endif; ?>
                <?php if($this->session->flashdata('error_foto_ktp')): ?>
                <div class="alert alert-danger" role="alert">
                  <?php echo $this->session->flashdata('error_foto_ktp'); ?>
                </div>
                <?php endif; ?>
                <?php if($this->session->flashdata('sent')): ?>
                <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('sent'); ?>
                </div>
                <?php endif; ?>
                <p style="font-size: 12px"><mark>ket: <span style="color: red">*</span> Tidak boleh kosong</mark></p>
                <div class="form-group">
                  <label class="col-xs-12"> Nama Lengkap<span style="color: red"> *</span></label>
                  <div class="col-xs-12 ">
                    <input class="form-control" maxlength="30" name="nm_lengkap" placeholder="Masukan nama anda" style="margin-bottom: 10px" type="text" />
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-xs-12"> Jenis Kelamin<span style="color: red"> *</span></label>
                  <div class="col-xs-12 "  style="margin-bottom: 10px">
                   <label class="radio-inline"> <input type="radio" name="jns_klmn" id="id_gender_1" value="Pria"  style="margin-bottom: 10px">Pria</label>
                   <label class="radio-inline"> <input type="radio" name="jns_klmn" id="id_gender_2" value="Wanita"  style="margin-bottom: 10px">Wanita </label>
                 </div>
               </div>

               <div class="form-group">
                <label class="col-xs-12"> E-mail<span style="color: red"> *</span></label>
                <div class="col-xs-12 ">
                  <input class="form-control" name="email" placeholder="Masukan email anda" style="margin-bottom: 10px" type="email" />
                </div>     
              </div>

              <div class="form-group">
                <label class="col-xs-12"> Tanggal Lahir<span style="color: red"> *</span></label>
                <div class="col-xs-12 ">
                  <input class="form-control" type="date" name="tgl_lahir" style="margin-bottom: 10px">
                </div>
              </div>

              <div class="form-group">
               <label class="col-xs-12"> No. Handphone<span style="color: red"> *</span></label>
               <div class="col-xs-12 ">
                <input class="form-control" name="no_hp" placeholder="Masukan Nomor handphone anda" style="margin-bottom: 10px" type="text" />
              </div> 
            </div>

            <div class="form-group">
             <label class="col-xs-12"> No. Handphone kerabat<span style="color: red"> *</span></label>
             <div class="col-xs-12 ">
              <input class="form-control" name="no_hp_ortu1" placeholder="Masukan Nomor handphone kerabat 1" style="margin-bottom: 10px" type="text" />
              <input class="form-control" name="no_hp_ortu2" placeholder="Masukan Nomor handphone kerabat 2" style="margin-bottom: 10px" type="text" />
            </div> 
          </div>

          <div class="form-group"> 
            <label class="col-xs-12"> Alamat Lengkap<span style="color: red"> *</span></label> 
            <div class="col-xs-12 "> 
              <textarea class="input-xs textinput textInput form-control" id="alamat" name="alamat" placeholder="Masukan alamat lengkap anda" style="margin-bottom: 10px" type="text"></textarea>
            </div>
          </div>

          <div class="default-file-upload">
            <label class="col-xs-12"> Upload Ijazah / Buku nikah asli<span style="color: red"> *</span></label>
            <div class="col-xs-12 " style="margin-bottom: 10px">  
              <input id="file-upload1" name="lampiran" type="file"/>
            </div>
          </div>

          <div class="default-file-upload">
            <label class="col-xs-12"> Photocopy KTP<span style="color: red"> *</span></label>
            <div class="col-xs-12 " style="margin-bottom: 10px">  
              <input id="file-upload2" name="foto_ktp" type="file"/>
            </div>
          </div>

          <div class="form-group"> 
            <div class="col-xs-12 ">
              <input type="submit" name="btnSubmit" value="Submit" class="btn btn-primary btn btn-danger" />
            </div>
          </div> 
      </form>
    </div>
  </div>
</div> 

</div>
<!-- akhir formnya -->

</div>     
<footer class="container-fluid">
  <div class="row">
    <div class="col-xs-12">
      <p><b>
        © 2019 LPK Pancaran Kasih - All right reserved.
      </b></p>
    </div>
  </div>
</footer>
</body>