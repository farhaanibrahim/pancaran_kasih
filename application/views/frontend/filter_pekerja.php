<!DOCTYPE html>
<html lang="en">
<head>
  <title>LPK Pancaran Kasih-Cari Pekerja</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/bootstrap.min.js"></script>
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/plugin/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="73x73" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-73-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/favicon.png" />
  <style>
  .navbar {
    background:#34793A;
    border-color: transparent;
    margin-bottom: 0;
    border-radius: 0;
    text-shadow: 1px 1px 1px #000
  }
  .navbar li a, .navbar {
    color: #93f765 !important;
    font-size: 11px
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #34793A !important;
    background-color: #93f765 !important;
    text-shadow: 1px 0.5px 0.5px #000
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  @media (min-width: 768px) {
    .navbar-nav.navbar-center {
      position: absolute;
      left: 50%;
      transform: translatex(-50%);
    }
  }

  footer {
    background: #000; 
    color:#fff;
    padding-top: 30px;
  }
  /* Float four columns side by side */
  .column {
    float: left;
    width: 25%;
    padding: 0 10px;
  }

  /* Remove extra left and right margins, due to padding */
  .row {margin: 0 -5px;}

  /* Clear floats after the columns */
  .row:after {
    content: "";
    display: table;
    clear: both;
  }

  /* Responsive columns */
  @media screen and (max-width: 600px) {
    .column {
      width: 100%;
      display: block;
      margin-bottom: 20px;
    }
  }

  /* Style the counter cards */
  .card {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    padding: 16px;
    text-align: center;
    background-color: #34793A;
    color: #93f765;
  }
</style>
</head>
<body>

  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="background: #93f765">
          <span style="background: #34793A" class="icon-bar"></span>
          <span style="background: #34793A" class="icon-bar"></span>
          <span style="background: #34793A" class="icon-bar"></span>                     
        </button>
        <a href="<?php echo base_url(); ?>" class="navbar-brand" style="color: #93f765; border-bottom: 3px solid #93f765">LPK Pancaran Kasih</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-center">
          <li><a href="<?php echo base_url(); ?>home">HOMEPAGE</a></li>
          <li><a href="<?php echo base_url(); ?>gallery">GALLERY</a></li>
          <li><a href="<?php echo base_url(); ?>loker">LOKER</a></li>
          <li class="active"><a href="<?php echo base_url(); ?>pekerja">CARI PEKERJA</a></li>
          <li><a href="<?php echo base_url(); ?>artikel">ARTIKEL</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo base_url(); ?>kontak"><span class="glyphicon glyphicon-envelope"></span> KONTAK</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container" style="padding-top: 70px;padding-bottom: 30px">
  <div style="padding-bottom: 30px; color: #34792a;text-shadow: 1px 1px 0px #000"><h2 class="text-center">Cari Pekerja</h2></div>
    <div style="background-color: #34792A;color: #93F765;text-shadow: 1px 0.5px 0.5px #000; padding: 30px">
      <div class="container-fluid ">
        <div class="row align-items-center justify-content-center">
        <form action="<?php echo base_url('pekerja/filter'); ?>" method="post" enctype="multipart/form-data">

          <div class="col-md-3 col-xs-6">
            <div class="form-group ">
              <select id="inputState" name="jns_pekerjaan" class="form-control">
                <option value="-" selected>Jenis Pekerjaan</option>
                <?php foreach($jns_pekerjaan->result() as $jns_pekerjaan): ?>
                  <option value="<?php echo $jns_pekerjaan->id_pekerjaan; ?>"><?php echo $jns_pekerjaan->nm_pekerjaan; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

          <div class="col-md-3 col-xs-6">
            <div class="form-group ">
              <select id="inputState" name="jns_kelamin" class="form-control">
                <option value="-" selected>Jenis Kelamin</option>
                <option value="pria">Pria</option>
                <option value="wanita">Wanita</option>
              </select>
            </div>
          </div>

          <div class="col-md-3 col-xs-6">
            <div class="form-group ">
              <select id="inputState" name="pendidikan" class="form-control">
                <option value="-" selected>Pendidikan</option>
                <option value="diploma">Diploma</option>
                <option value="sarjana">Sarjana</option>
                <option value="sd">SD</option>
                <option value="slta">SLTA</option>
                <option value="sltp">SLTP</option>
              </select>
            </div>
          </div>

          <div class="col-md-3 col-xs-6">
            <div class="form-group ">
              <select id="inputState" name="agama" class="form-control">
                <option value="-" selected>Agama</option>
                <option value="islam">Islam</option>
                <option value="katholik">Katholik</option>
                <option value="protestan">Protestan</option>
                <option value="hindu">Hindu</option>
                <option value="budha">Budha</option>
              </select>
            </div>
          </div>

          <div class="col-md-3 col-xs-6">
            <div class="form-group ">
              <select id="inputState" name="status" class="form-control">
                <option value="-" selected>Status</option>
                <?php foreach($status->result() as $status): ?>
                  <option value="<?php echo $status->id_status; ?>"><?php echo $status->nm_status; ?></option>
                <?php endforeach; ?>
              </select>
            </div>  
          </div>

          <div class="col-md-3 col-xs-6">
            <div class="form-group">
              <select class="form-control" name="suku">
                <option value="-" selected>Suku</option>
                <?php foreach($ethnic->result() as $ethnic): ?>
                  <option value="<?php $ethnic->id_suku; ?>"><?php echo $ethnic->nm_suku; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

          <div class="col-md-3 col-xs-6">
            <div class="form-group">
              <select class="form-control" name="umur">
                <option value="-">Umur</option>
                <option value="17">17-21 thn</option>
                <option value="22">22-30 thn</option>
                <option value="31">31-50 thn</option>
                <option value="50">50 tahun keatas</option>
              </select>
            </div>
          </div>

          <div class="col-md-3 col-xs-6">
            <div class="form-group">
              <select class="form-control" name="status_kawin">
                <option value="-" selected>Status Kawin</option>
                <option>Married</option>
                <option>Single</option>
              </select>
            </div>
          </div>

          <div class="col-md-3 col-xs-6">
            <div class="form-group">
              <input type="submit" value="Filter" class="btn btn-success">
            </div>
          </div>

          </form>
        </div>
      </div>
    </div>
  </div>

<hr>

<div class="container" style="padding-bottom: 30px;padding-top: 30px">
  <div class="row" style="padding-bottom: 30px">
    <?php if($sdm->num_rows() == 0): ?>
      <div class="alert alert-warning" role="alert">
        Data tidak ditemukan.
      </div>
    <?php endif; ?>
    <?php foreach($sdm->result() as $sdm): ?>
      <div class="column">
        <div class="card" style="margin-bottom: 30px; height: 400px;">
          <a href="<?php echo base_url(); ?>pekerja/order/<?php echo $sdm->id_worker ?>"><img src="<?php echo base_url('uploads'); ?>/<?php echo $sdm->foto; ?>" alt="Avatar" style="width:100%"></a></a>
          <h3><?php echo $sdm->nm_lengkap; ?></h3>
          <p><?php echo $sdm->nm_pekerjaan; ?></p>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <div id="pagination">
    <?php if (isset($links)) {
        echo $links;
      } 
    ?>
  </div>
</div>

<footer class="container-fluid">
  <div class="row">
    <div class="col-xs-12">
      <p><span style="color: #93F765" class="glyphicon glyphicon-earphone"></span> (021) 82401313 - (021) 82428499</p>
      <p><span style="color: #93F765" class="glyphicon glyphicon-envelope"></span> lpkpancarankasih2006@gmail.com</p>
      <p><a target="_blank" href="https://www.facebook.com/adminpk20anni/"> <button style="border-radius: 8px;"><i style="color: blue" class="fab fa-facebook-f"></i> LPK Pancaran Kasih Bekasi</button></a></p>
    </div>
  </div>
  <div class="row" style="padding-top: 30px;text-align: center; ">
    <div class="col-xs-12">
      © 2019 LPK Pancaran Kasih - All right reserved.
    </div>
  </div>
</footer>

</body>