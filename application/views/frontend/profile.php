<?php foreach($sdm->result() as $sdm): ?>
<?php endforeach; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>LPK Pancaran Kasih-Artikel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/bootstrap.min.js"></script>
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/plugin/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="73x73" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-73-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/favicon.png" />
  <style>
  .navbar {
    background:#34793A;
    border-color: transparent;
    margin-bottom: 0;
    border-radius: 0;
    text-shadow: 1px 1px 1px #000
  }
  .navbar li a, .navbar {
    color: #93f765 !important;
    font-size: 11px
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #34793A !important;
    background-color: #93f765 !important;
    text-shadow: 1px 0.5px 0.5px #000
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  @media (min-width: 768px) {
    .navbar-nav.navbar-center {
      position: absolute;
      left: 50%;
      transform: translatex(-50%);
    }
  }

  footer {
    background: #000; 
    color:#fff;
    padding-top: 30px;
    padding-bottom: 30px
  }
  .user-row {
    margin-bottom: 14px;
  }

  .user-row:last-child {
    margin-bottom: 0;
  }

  .table-user-information > tbody > tr {
    border-top: 1px solid rgb(221, 221, 221);
  }

  .table-user-information > tbody > tr:first-child {
    border-top: 0;
  }


  .table-user-information > tbody > tr > td {
    border-top: 0;
  }
  .toppad
  {margin-top:20px;
  }
</style>
</head>
<body>
  <!-- ini mulai navbar -->
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="background: #93f765">
          <span style="background: #34793A" class="icon-bar"></span>
          <span style="background: #34793A" class="icon-bar"></span>
          <span style="background: #34793A" class="icon-bar"></span>                     
        </button>
        <a href="<?php echo base_url(); ?>" class="navbar-brand" style="color: #93f765; border-bottom: 3px solid #93f765">LPK Pancaran Kasih</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-center">
          <li><a href="<?php echo base_url(); ?>">HOMEPAGE</a></li>
          <li><a href="<?php echo base_url(); ?>gallery">GALLERY</a></li>
          <li><a href="<?php echo base_url(); ?>loker">LOKER</a></li>
          <li class="active"><a href="<?php echo base_url(); ?>pekerja">CARI PEKERJA</a></li>
          <li><a href="<?php echo base_url(); ?>artikel">ARTIKEL</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo base_url(); ?>kontak"><span class="glyphicon glyphicon-envelope"></span> KONTAK</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- akhir navbar -->

<!-- mulai isinya -->
  <div class="container-fluid" style="padding-top: 50px">
    <div class="row">
      <div class="col-xs-12">
       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >

        <!-- mulai tombol kembali -->
        <ul class="pager">
          <li class="previous"><a href="<?php echo base_url(); ?>pekerja">&larr; Kembali</a></li>
        </ul> 
        <!-- akhir tombol kembali -->

        <?php if($this->session->flashdata('sent')): ?>
          <div class="alert alert-success" role="alert">
            <?php echo $this->session->flashdata('sent'); ?>
          </div>
        <?php endif; ?>

        <!-- mulai profilenya dalam bentuk card -->
        <div class="panel panel-info" style="border-color: #34792A;">
          <div class="panel-heading" style="background-color: #34792A;color: #93F765;text-shadow: 1px 0.5px 0.5px #000">
            <h3 class="panel-title"><?php echo $sdm->nm_lengkap; ?> <mark><?php echo $sdm->nm_pekerjaan; ?></mark></h3>
          </div>
          <!-- badannya card dimulai -->
          <div class="panel-body">
            <div class="row">
              <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="<?php echo base_url('assets/frontend'); ?>/assets/img/pekerja/1.png" class="img-circle img-responsive"> </div>
              <div class=" col-md-9 col-lg-9 "> 
                <table class="table table-user-information">
                  <tbody>
                    <tr>
                      <td>Nama</td>
                      <td>: <?php echo $sdm->nm_lengkap; ?></td>
                    </tr>
                    <tr>
                      <td>Profesi</td>
                      <td>: <?php echo $sdm->nm_pekerjaan; ?></td>
                    </tr>
                    <tr>
                      <td>Tinggi & Berat Badan</td>
                      <td>: <?php echo $sdm->tb." Cm"; ?> & <?php echo $sdm->bb." Kg"; ?></td>
                    </tr>
                    <tr>
                      <td>Jenis Kelamin</td>
                      <td>: <?php echo $sdm->jns_kelamin; ?></td>
                    </tr>
                    <tr>
                      <td>Agama</td>
                      <td>: <?php echo $sdm->agama; ?></td>
                    </tr>
                    <tr>
                      <td>Pendidikan</td>
                      <td>: <?php echo $sdm->pendidikan; ?></td>
                    </tr>

                    <tr>
                      <td>Status Kawin</td>
                      <td>: <?php echo $sdm->status_kawin; ?></td>
                    </tr>
                    <tr>
                      <td>Memiliki Anak</td>
                      <td>: <?php echo $sdm->punya_anak; ?></td>
                    </tr>
                    <tr>
                      <td>Asal</td>
                      <td>: <?php echo $sdm->asal; ?></td>
                    </tr>
                    <tr>
                      <td>Suku</td>
                      <td>: <?php echo $sdm->suku; ?></td>
                    </tr>
                    <tr>
                      <td>Domisili</td>
                      <td>: <?php echo $sdm->domisili; ?></td>
                    </tr>
                    <tr>
                      <td>Umur</td>
                      <td>: <?php echo $sdm->umur; ?></td>
                    </tr>
                    <tr>
                      <td>Status</td>
                      <td>: <?php echo $sdm->status; ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- akhri badannya card -->

            <!-- mulai kaki cardnya -->
            <div class="panel-footer">
              <div class="row">
                <div class="col-xs-12 col-md-6">
                  <h4> Keahlian</h4>
                  <p><?php echo $sdm->keahlian; ?></p>
                </div>
                <div class="col-xs-12 col-md-6">
                  <h4> Pengalaman</h4>
                  <ul>
                  <?php echo $sdm->pengalaman; ?>
                  </ul>
                </div>
              </div>
            </div>
            <!-- akhri kaki cardnya -->
          </div>
        </div>
        <!-- akhir dari profile yg bentuk card -->
      </div>
    </div>
  </div>
</div>
<!-- akhir isinya -->

<hr>
<!-- mulai bagian form pemesanannya -->
<div class="container-fluid" style="padding-bottom: 30px">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">
      <div class="panel panel-info" style="border-color: #34792A;">
        <div class="panel-heading" style="background-color: #34792A;color: #93F765;text-shadow: 1px 0.5px 0.5px #000">
          <div class="panel-title text-center">Form Untuk Memesan Pekerja</div>
        </div>  
        <div class="panel-body">
            <form  class="form-horizontal" method="post" action="<?php echo base_url('pekerja/order'); ?>/<?php echo $sdm->id_worker ?>" enctype="multipart/form-data">
              <input type="hidden" name="id_worker" value="<?php echo $sdm->id_worker ?>">
              <p style="font-size: 12px"><mark>ket: <span style="color: red">*</span> Tidak boleh kosong</mark></p>
              <div class="form-group">
                <label class="col-xs-12"> Nama Lengkap<span style="color: red"> *</span></label>
                <div class="col-xs-12 ">
                  <input class="form-control" maxlength="30" name="nama" placeholder="Masukan nama anda" style="margin-bottom: 10px" type="text" />
                </div>
              </div>

              <div class="form-group">
                <label class="col-xs-12"> E-mail<span style="color: red"> *</span></label>
                <div class="col-xs-12 ">
                  <input class="form-control" name="email" placeholder="Masukan email anda" style="margin-bottom: 10px" type="email" />
                </div>     
              </div>

              <div class="form-group">
               <label class="col-xs-12"> No. Handphone<span style="color: red"> *</span></label>
               <div class="col-xs-12 ">
                <input class="form-control" name="no_hp" placeholder="Masukan Nomor handphone anda" style="margin-bottom: 10px" type="text" />
              </div> 
            </div>

            <div class="form-group"> 
              <label class="col-xs-12"> Alamat Lengkap<span style="color: red"> *</span></label> 
              <div class="col-xs-12 "> 
                <textarea class="input-xs textinput textInput form-control" id="alamat" name="alamat" placeholder="Masukan alamat lengkap anda" style="margin-bottom: 10px" type="text"></textarea>
              </div>
            </div>

            <div class="form-group"> 
              <label class="col-xs-12"> Keterangan Lain<span style="color: red"> *</span></label> 
              <div class="col-xs-12 "> 
                <textarea class="input-xs textinput textInput form-control" id="keterangan" name="keterangan" placeholder="Keterangan Lainnya" style="margin-bottom: 10px" type="text"></textarea>
              </div>
            </div>

           <div class="form-group"> 
            <div class="col-xs-12 ">
              <input type="submit" name="btnSubmit" value="Kirim Pesanan" class="btn btn-primary btn btn-danger" />
            </div>
          </div> 
        </form>
    </div>
  </div>
</div>
</div>
</div>
<!-- akhir dari bagian formnya -->

<!-- mulai bagian footernya -->
<footer class="container-fluid">
  <div class="row">
    <div class="col-xs-12">
      <p><span style="color: #93F765" class="glyphicon glyphicon-earphone"></span> (021) 82401313 - (021) 82428499</p>
      <p><span style="color: #93F765" class="glyphicon glyphicon-envelope"></span> lpkpancarankasih2006@gmail.com</p>
      <p><a target="_blank" href="https://www.facebook.com/adminpk20anni/"> <button style="border-radius: 8px;"><i style="color: blue" class="fab fa-facebook-f"></i> LPK Pancaran Kasih Bekasi</button></a></p>
    </div>
  </div>
  <div class="row" style="padding-top: 30px;text-align: center; ">
    <div class="col-xs-12">
      © 2019 LPK Pancaran Kasih - All right reserved.
    </div>
  </div>
</footer>
<!-- akhir dari footernya -->
</body>
</html>