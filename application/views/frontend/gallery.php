<!DOCTYPE html>
<html lang="en">
<head>
  <title>LPK Pancaran Kasih-Galerry</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/bootstrap.min.js"></script>
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/plugin/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/favicon.png" />
  <style>
  .navbar {
    background:#34792A;
    border-color: transparent;
    margin-bottom: 0;
    border-radius: 0;
    text-shadow: 1px 1px 1px #000
}
.navbar li a, .navbar {
    color: #93f765 !important;
    font-size: 11px
}
.navbar-nav li a:hover, .navbar-nav li.active a {
    color: #34792A !important;
    background-color: #93f765 !important;
    text-shadow: 1px 0.5px 0.5px #000
}
.navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
}
@media (min-width: 768px) {
    .navbar-nav.navbar-center {
      position: absolute;
      left: 50%;
      transform: translatex(-50%);
  }
}

footer {
    background: #000; 
    color:#fff;
    padding-top: 20px;
    padding-bottom: 10px
}
div.gallery {
  border: 1px solid #ccc;
}

div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
}

* {
  box-sizing: border-box;
}

.responsive {
  padding: 0 6px;
  float: left;
  width: 24.99999%;
}

@media only screen and (max-width: 700px) {
  .responsive {
    width: 49.99999%;
    margin: 6px 0;
}
}

@media only screen and (max-width: 500px) {
  .responsive {
    width: 100%;
}
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
</style>
</head>
<body>
  <!-- awal nav -->
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="background: #93f765">
          <span style="background: #34792A" class="icon-bar"></span>
          <span style="background: #34792A" class="icon-bar"></span>
          <span style="background: #34792A" class="icon-bar"></span>                        
      </button>
      <a href="<?php echo base_url(); ?>" class="navbar-brand" style="color: #93f765; border-bottom: 3px solid #93f765">LPK Pancaran Kasih</a>
  </div>
  <div class="collapse navbar-collapse" id="myNavbar">
    <ul class="nav navbar-nav navbar-center">
        <li><a href="<?php echo base_url(); ?>">HOMEPAGE</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>gallery">GALLERY</a></li>
        <li><a href="<?php echo base_url(); ?>loker">LOKER</a></li>
        <li><a href="<?php echo base_url(); ?>pekerja">CARI PEKERJA</a></li>
        <li><a href="<?php echo base_url(); ?>artikel">ARTIKEL</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url(); ?>kontak"><span class="glyphicon glyphicon-envelope"></span> KONTAK</a></li>
  </ul>
</div>
</div>
</nav>
<!-- akhir nav -->

<!-- mulai  foto galeri 4x3 -->
<div class="container-fluid" style="padding-top: 70px;padding-bottom: 20px">
  <div><h2 style="color: #34792a;text-shadow: 1px 1px 0px #000" class="text-center">GALLERY</h2></div><br>
  <div class="row" style="padding-top: 10px">

    <?php foreach($gallery->result() as $gallery): ?>
      <div class="responsive">
        <div class="gallery">
          <a target="_blank" href="<?php echo base_url('uploads'); ?>/<?php echo $gallery->file; ?>">
            <img src="<?php echo base_url('uploads'); ?>/<?php echo $gallery->file; ?>" data-lightbox="baris1" data-title="title" style="width:100%">
          </a>
          <div class="desc"><?php echo $gallery->title; ?></div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
<!-- akhir  foto galeri 4x3 -->

<!-- mulai copyrightnya -->
  <footer class="container-fluid">
    <div class="row">
      <div class="col-xs-12">
        <p><span style="color: #93F765" class="glyphicon glyphicon-earphone"></span> (021) 82401313 - (021) 82428499</p>
        <p><span style="color: #93F765" class="glyphicon glyphicon-envelope"></span> lpkpancarankasih2006@gmail.com</p>
        <p><a target="_blank" href="https://www.facebook.com/adminpk20anni/"> <button style="border-radius: 8px;"><i style="color: blue" class="fab fa-facebook-f"></i> LPK Pancaran Kasih Bekasi</button></a></p>
      </div>
    </div>
    <div class="row" style="padding-top: 30px;text-align: center; ">
      <div class="col-xs-12">
        © 2019 LPK Pancaran Kasih - All right reserved.
      </div>
    </div>
  </footer>
<!-- akhir copyright -->

</body>