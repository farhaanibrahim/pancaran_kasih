<!DOCTYPE html>
<html lang="en">
<head>
  <title>LPK Pancaran Kasih</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/bootstrap.min.js"></script>
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/plugin/lightbox/css/lightbox.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/plugin/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/favicon.png" />
  <style>

  .navbar {
    background:#34792A;
    border-color: transparent;
    margin-bottom: 0;
    border-radius: 0;
    text-shadow: 1px 1px 1px #000
  }
  .navbar li a, .navbar {
    color: #93f765 !important;
    font-size: 12px;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #34792A !important;
    background-color: #93f765 !important;
    text-shadow: 1px 0.5px 0.5px #000
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  @media (min-width: 768px) {
    .navbar-nav.navbar-center {
      position: absolute;
      left: 50%;
      transform: translatex(-50%);
    }
  }

  footer {
    background: #000; 
    color:#fff;
    
    padding-top: 20px;
    padding-bottom: 10px
  }

  .jumbotron{
    height: 800px;
    background:#000;
    background-image: url(<?php echo base_url('assets/frontend'); ?>/assets/img/bn.png);
    background-position: center;
    background-size: cover;
  }

  h2{
    color: #34792A;
    text-shadow: 1px 1px 0px #000
  }
  .slideanim {visibility:hidden;}
  .slide {
    animation-name: slide;
    -webkit-animation-name: slide;
    animation-duration: 1s;
    -webkit-animation-duration: 1s;
    visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
      width: 100%;
      margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
      font-size: 150px;
    }
  }
  .logo-small {
    color: #34792A;
    font-size: 50px;
  }
  .logo {
    color: #34792A;
    font-size: 200px;
  }
</style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

  <!-- awal nav -->
  <a href="#" id="scroll" style="display: none;"></a>
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="background: #93f765">
          <span style="background: #34792A" class="icon-bar"></span>
          <span style="background: #34792A" class="icon-bar"></span>
          <span style="background: #34792A" class="icon-bar"></span>                        
        </button>
        <a href="<?php echo base_url(); ?>" class="navbar-brand" style="color: #93f765; border-bottom: 3px solid #93f765">LPK Pancaran Kasih</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-center">
          <li><a href="#ourvalues">OUR VALUES</a></li>
          <li><a href="#jasa">JASA</a></li>
          <li><a href="#prosedur">PROSEDUR</a></li>
          <li><a href="<?php echo base_url(); ?>gallery">GALLERY</a></li>
          <li><a href="<?php echo base_url(); ?>loker">LOKER</a></li>
          <li><a href="<?php echo base_url(); ?>pekerja">CARI PEKERJA</a></li>
          <li><a href="<?php echo base_url(); ?>artikel">ARTIKEL</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo base_url(); ?>kontak"><span class="glyphicon glyphicon-envelope"></span> KONTAK</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- akhir nav -->

  <!-- ini jumboron -->
  <div class="jumbotron">
    <div class="container text-center" style="padding-top: 230px; color:#93f765;text-shadow: 2px 2px 2px #000;">
      <h1>LPK Pancaran Kasih - Penyalur Babysitter & ART</h1><br>      
      <br>
    </div>
  </div>
  <!-- akhir jumbotron -->

  <!-- ini visi,misinya dan knp milih kami -->
  <div  id="ourvalues" class="container class="container" style="padding-top: 100px; padding-bottom: 100px;">
    <div class="row">
      <div class="col-xs-12 text-center">
        <h2>VISI DAN MISI KAMI</h2>
        <br><br><br>
      </div>
      <div class="col-xs-12 col-md-6 slideanim" style="line-height: 1.5em;">
        <h4>
          <strong>VISI:</strong> <br><br>
          <ul style="line-height: 1.5em;">
            <li>Menciptakan Lapangan kerja</li>
            <li>Melatih dah Menyalurkan Tenaga Kerja</li>
            <li>Melindungi Konsumen dan Pekerja</li>
          </ul>
        </h4>
      </div>
      <div class="col-xs-12 col-md-6 slideanim" style="line-height: 1.5em;">
        <h4>
          <strong>MISI:</strong> <br><br>
          <ul style="line-height: 1.5em;">
            <li>Menciptakan Lapangan kerja</li>
            <li>Melatih dah Menyalurkan Tenaga Kerja</li>
            <li>Melindungi Konsumen dan Pekerja</li>
          </ul>
        </h4>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-xs-12 text-center">
        <h2>Mengapa Memilih Kami?</h2>
        <br><br><br>
      </div>
      <div class="row slideanim text-center" >
        <div class="col-xs-12 col-md-3" style="line-height: 1.5em;">
          <span class="glyphicon glyphicon-briefcase logo-small"></span>
          <h4>Professional </h4>
          <p>Pekerja di LPK Pancaran Kasih terlatih, melalui tahap seleksi dan pelatihan.</p>
        </div>
        <div class="col-xs-12 col-md-3" style="line-height: 1.5em;">
          <span class="glyphicon glyphicon-blackboard logo-small"></span>
          <h4>Monitoring </h4>
          <p>LPK Pancaran Kasih akan selalu memonitor kerja para pekerja melalui kunjungan atau komunikasi.</p>
        </div>
        <div class="col-xs-12 col-md-3" style="line-height: 1.5em;">
          <span class="glyphicon glyphicon-piggy-bank logo-small"></span>
          <h4>Terjangkau</h4>
          <p>LPK Pancaran Kasih memberikan penawaran terbaik dan terjangkau dibanding sius media iklan lainnya. Anda dapat memesan pekerja sesuai dengan kriteria pilihan anda.</p>
        </div>
        <div class="col-xs-12 col-md-3" style="line-height: 1.5em;">
          <span class="glyphicon glyphicon-time logo-small"></span>
          <h4>Layanan 24 Jam</h4>
          <p>Anda dapat mengakses situs LPK Pancaran Kasih kapanpun dan dimanapun tanpa mengganggu jam kerja anda. Pesanan anda akan diproses secepat mungkin.</p>
        </div>
      </div>
    </div>
  </div><br><br><br><br>
  <!-- ini akhir visi,misi dan knp milih kami -->

  <!-- ini awal jasa yg ditawrkan -->
  <div class="container" id="jasa" style="padding-bottom: 200px ;padding-top: 30px">
    <hr style="height:1px;border:none;color:#333;background-color:#333;">
    <div class="row slideanim">
      <h2 class="text-center" style="padding-top: 70px;padding-bottom: 80px">Jasa Yang Kami Tawarkan</h2>

      <div class="col-xs-12 col-md-3">
        <img src="<?php echo base_url('assets/frontend'); ?>/assets/img/jenis_pekerjaan/baby_sitter.jpg" width="300" height="200 ">
        <br><br>
        <p align="center"><strong>Perawat</strong></p>
      </div>
      <div class="col-xs-12 col-md-3">
        <img src="<?php echo base_url('assets/frontend'); ?>/assets/img/jenis_pekerjaan/art.jpg" width="300" height="200 ">
        <br><br>
        <p align="center"><strong>Asisten Rumah Tangga</strong></p>
      </div>
      <div class="col-xs-12 col-md-3">
        <img src="<?php echo base_url('assets/frontend'); ?>/assets/img/jenis_pekerjaan/tukang_kebun.jpg" width="300" height="200 ">
        <br><br>
        <p align="center"><strong>Tukang Kebun</strong></p>
      </div>
      <div class="col-xs-12 col-md-3">
        <img src="<?php echo base_url('assets/frontend'); ?>/assets/img/jenis_pekerjaan/driver.jpg" width="300" height="200 ">
        <br><br>
        <p align="center"><strong>Supir Pribadi</strong></p>
      </div>
    </div>
  </div>
  <!-- ini akhir jasa yg ditawarkan -->


  <!-- ini awal prosedur yg ditawrkan -->
  <div class="container" id="prosedur" style="padding-bottom: 70px ;padding-top: 30px">
    <hr style="height:1px;border:none;color:#333;background-color:#333;">
    <div class="row slideanim">

      <div class="text-center" style="padding-top: 60px">
        <h2>Prosedur</h2>
        <h4>Berikut adalah prosedur pemesanan</h4>
      </div>

      <div class="col-xs-12">
        <img src="<?php echo base_url('assets/frontend'); ?>/assets/img/flow.png" class="img-responsive" width="100%" height="600 ">
      </div>

    </div>
  </div>
  <!-- ini akhir prosedur yg ditawarkan -->

  <!-- ini awal berita baru -->
  <div class="container" style="padding-bottom: 50px">
    <hr style="height:1px;border:none;color:#333;background-color:#333;">
    <div class="row slideanim">

      <div class="text-center" style="padding-top: 50px;padding-bottom: 80px">
        <h2>Berita Terbaru</h2>
      </div>

      <?php foreach($article->result() as $article): ?>
        <div class="col-xs-12 col-md-3">
          <img src="<?php echo base_url('uploads'); ?>/<?php echo $article->foto; ?>" width="300" height="200 ">
          <br><br>
          <p align="center"><strong><a href="<?php echo base_url('artikel/detail'); ?>/<?php echo $article->url_slug; ?>"><?php echo $article->judul; ?></a></strong></p>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
  <!-- ini akhir berita baru -->

  <!-- ini awal galeri yg ditawrkan -->
  <div class="container" style="padding-bottom: 200px ;padding-top: 30px">
    <hr style="height:1px;border:none;color:#333;background-color:#333;">
    <div class="row slideanim">

      <div class="text-center" style="padding-top: 30px;padding-bottom: 20px">
        <h2>Galeri</h2>
      </div>
      <?php foreach($gallery->result() as $gallery): ?>
        <div class="col-xs-12 col-md-3">
          <img src="<?php echo base_url('uploads'); ?>/<?php echo $gallery->file; ?>" width="300" height="200 ">
          <br><br><br>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
  <!-- ini akhir galeri yg ditawarkan -->

  <!-- mulai copyrightnya -->
  <footer class="container-fluid">
    <div class="row">
      <div class="col-xs-12">
        <p><span style="color: #93F765" class="glyphicon glyphicon-earphone"></span> (021) 82401313 - (021) 82428499</p>
        <p><span style="color: #93F765" class="glyphicon glyphicon-envelope"></span> lpkpancarankasih2006@gmail.com</p>
        <p><a target="_blank" href="https://www.facebook.com/adminpk20anni/"> <button style="border-radius: 8px;"><i style="color: blue" class="fab fa-facebook-f"></i> LPK Pancaran Kasih Bekasi</button></a></p>
      </div>
    </div>
    <div class="row" style="padding-top: 30px;text-align: center; ">
      <div class="col-xs-12">
        © 2019 LPK Pancaran Kasih - All right reserved.
      </div>
    </div>
  </footer>
  <!-- akhir copyright -->
  <script>
    $(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

   // Make sure this.hash has a value before overriding default behavior
   if (this.hash !== "") {

    // Prevent default anchor click behavior
    event.preventDefault();

    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 900, function(){

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
    });
    } // End if
  });
})
</script> 
<script>
  $(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
      if (pos < winTop + 600) {
        $(this).addClass("slide");
      }
    });
  });
})
</script>
</body>
</html>
