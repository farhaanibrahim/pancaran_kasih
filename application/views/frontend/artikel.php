<!DOCTYPE html>
<html lang="en">
<head>
  <title>LPK Pancaran Kasih-Artikel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/bootstrap.min.js"></script>
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/plugin/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="73x73" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-73-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/favicon.png" />
  <style>
  .navbar {
    background:#34793A;
    border-color: transparent;
    margin-bottom: 0;
    border-radius: 0;
    text-shadow: 1px 1px 1px #000
  }
  .navbar li a, .navbar {
    color: #93f765 !important;
    font-size: 11px
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #34793A !important;
    background-color: #93f765 !important;
    text-shadow: 1px 0.5px 0.5px #000
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  @media (min-width: 768px) {
    .navbar-nav.navbar-center {
      position: absolute;
      left: 50%;
      transform: translatex(-50%);
    }
  }

  footer {
    background: #000; 
    color:#fff;
    padding-top: 30px;
    padding-bottom: 30px
  }
  p{
    text-align: justify;
  }
</style>
</head>
<body>
  <!-- mulai nav disini -->
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="background: #93f765">
          <span style="background: #34793A" class="icon-bar"></span>
          <span style="background: #34793A" class="icon-bar"></span>
          <span style="background: #34793A" class="icon-bar"></span>                     
        </button>
        <a href="<?php echo base_url(); ?>" class="navbar-brand" style="color: #93f765; border-bottom: 3px solid #93f765">LPK Pancaran Kasih</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-center">
          <li><a href="<?php echo base_url(); ?>home">HOMEPAGE</a></li>
          <li><a href="<?php echo base_url(); ?>gallery">GALLERY</a></li>
          <li><a href="<?php echo base_url(); ?>loker">LOKER</a></li>
          <li><a href="<?php echo base_url(); ?>pekerja">CARI PEKERJA</a></li>
          <li class="active"><a href="<?php echo base_url(); ?>artikel">ARTIKEL</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo base_url(); ?>kontak"><span class="glyphicon glyphicon-envelope"></span> KONTAK</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- akhir nav disini -->

<!-- mulai badan web disini -->
  <div class="container" style="padding-top: 70px; padding-bottom: 20px">
    <div class="row">
      <div class="col-md-8">

        <h1>Artikel Terbaru</h1>
        <!-- mulai artikel -->
        <?php foreach($article->result() as $row): ?>
          <article>
            <h2><a href="<?php echo base_url(); ?>artikel/detail/<?php echo $row->url_slug; ?>"><?php echo $row->judul; ?></a></h2>

            <div class="row">
              <div class="col-sm-6 col-md-6">
                <span class="glyphicon glyphicon-time"></span> <?php echo $row->tgl; ?>                    
              </div>
            </div>

            <hr>

            <img src="<?php echo base_url('uploads'); ?>/<?php echo $row->foto; ?>" class="img-responsive">

            <br />

            <p><?php echo substr($row->isi,0,800); ?></p>

            <p class="text-right">
              <a href="<?php echo base_url(); ?>artikel/detail/<?php echo $row->url_slug; ?>">
                continue reading...
              </a>
            </p>

            <hr>
          </article>
        <?php endforeach; ?>

        <div id="pagination">
          <?php if (isset($links)) {
              echo $links;
            } 
          ?>
        </div>

      </div>

      <!-- mulai widgetnya -->
      <div class="col-md-4">

        <!-- mulai cari artikel -->
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #34792A;color: #93F765;text-shadow: 1px 0.5px 0.5px #000">
            <h4>Cari Disini</h4>
          </div>
          <div class="card-body">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Cari artikel disini">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">Cari</button>
              </span>
            </div>
          </div>
        </div>
        <!-- akhir cari artikel -->

        <!-- mulai artikel terbaru -->
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #34792A;color: #93F765;text-shadow: 1px 0.5px 0.5px #000">
            <h4>Artikel terbaru</h4>
          </div>
          <ul class="list-group">
            <li class="list-group-item"><a href="singlepost.html">1. Aries Sun Sign March 21 - April 19</a></li>
            <li class="list-group-item"><a href="singlepost.html">2. Taurus Sun Sign April 20 - May 20</a></li>
            <li class="list-group-item"><a href="singlepost.html">3. Gemini Sun Sign May 21 - June 21</a></li>
            <li class="list-group-item"><a href="singlepost.html">4. Cancer Sun Sign June 22 - July 22</a></li>
            <li class="list-group-item"><a href="singlepost.html">5. Leo Sun Sign July 23 - August 22 </a></li>
          </ul>
        </div>
        <!-- akhir artikel terbaru -->

        <!-- mulai ketegori -->
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #34792A;color: #93F765;text-shadow: 1px 0.5px 0.5px #000">
            <h4>Kategori</h4>
          </div>
          <ul class="list-group">
            <li class="list-group-item"><a href="#">Signs</a></li>
            <li class="list-group-item"><a href="#">Elements</a></li>
            <li class="list-group-item"><a href="#">Planets</a></li>
            <li class="list-group-item"><a href="#">Cusps</a></li>
            <li class="list-group-item"><a href="#">Compatibility</a></li>
          </ul>
        </div>
        <!-- akhir kategori -->
      </div>
      <!-- akhir widgetnya -->
    </div>
  </div>
  <!-- akhir badan web disini -->

  <!-- mulai footer disini -->
<footer class="container-fluid">
  <div class="row">
    <div class="col-xs-12">
      <p><span style="color: #93F765" class="glyphicon glyphicon-earphone"></span> (021) 82401313 - (021) 82428499</p>
      <p><span style="color: #93F765" class="glyphicon glyphicon-envelope"></span> lpkpancarankasih2006@gmail.com</p>
      <p><a target="_blank" href="https://www.facebook.com/adminpk20anni/"> <button style="border-radius: 8px;"><i style="color: blue" class="fab fa-facebook-f"></i> LPK Pancaran Kasih Bekasi</button></a></p>
    </div>
  </div>
  <div class="row" style="padding-top: 30px;text-align: center; ">
    <div class="col-xs-12">
      © 2019 LPK Pancaran Kasih - All right reserved.
    </div>
  </div>
</footer>
  <!-- akhir footer disini -->
</body>
</html>