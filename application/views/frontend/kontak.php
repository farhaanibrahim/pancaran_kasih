<!DOCTYPE html>
<html lang="en">
<head>
  <title>LPK Pancaran Kasih-Artikel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/frontend'); ?>/assets/js/bootstrap.min.js"></script>
  <link href="<?php echo base_url('assets/frontend'); ?>/assets/plugin/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="73x73" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-73-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="<?php echo base_url('assets/frontend'); ?>/assets/ico/favicon.png" />
  <style>
  .navbar {
    background:#34793A;
    border-color: transparent;
    margin-bottom: 0;
    border-radius: 0;
    text-shadow: 1px 1px 1px #000
  }
  .navbar li a, .navbar {
    color: #93f765 !important;
    font-size: 11px
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #34793A !important;
    background-color: #93f765 !important;
    text-shadow: 1px 0.5px 0.5px #000
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  @media (min-width: 768px) {
    .navbar-nav.navbar-center {
      position: absolute;
      left: 50%;
      transform: translatex(-50%);
    }
  }

  footer {
    background: #000; 
    color:#fff;
    padding-top: 10px;
    padding-bottom: 30px
  }

  body{
    background-color: #93F765;
  }
  .contact-form{
    background: #fff;
    margin-bottom: 5%;
    width: 70%;
  }
  .contact-form form{
    padding: 14%;
  }
  .contact-form form .row{
    margin-bottom: -7%;
  }
  .contact-form h3{
    margin-bottom: 8%;
    margin-top: -10%;
    text-align: center;
    color: #34793A;
  }
  .contact-form .btnContact {
    border: none;
    padding: 1.5%;
    background: #34793A;
    font-weight: 600;
    color: #fff;
    cursor: pointer;
  }
  .btnContactSubmit
  {
    width: 50%;
    border-radius: 1rem;
    padding: 1.5%;
    color: #fff;
    background-color: #0062cc;
    border: none;
    cursor: pointer;
  }
  .card {
    box-shadow: 0 4px 8px 0 rgba(52,121,42,0.5);
    transition: 0.3s;
    background-color: #34793A;
    color: #fff;
    padding-left: 20px;
    padding-top: 1px;
    padding-bottom: 10px;
    padding-right:15px;
    margin-bottom: 15px;
  }
  .card:hover {
    box-shadow: 0 16px 32px 0 rgba(52,121,42,0.5);
  }
</style>
</head>
<body>
  <!-- mulai navnya -->
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="background: #93f765">
          <span style="background: #34793A" class="icon-bar"></span>
          <span style="background: #34793A" class="icon-bar"></span>
          <span style="background: #34793A" class="icon-bar"></span>                     
        </button>
        <a href="<?php echo base_url(); ?>" class="navbar-brand" style="color: #93f765; border-bottom: 3px solid #93f765">LPK Pancaran Kasih</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-center">
          <li><a href="<?php echo base_url(); ?>">HOMEPAGE</a></li>
          <li><a href="<?php echo base_url(); ?>gallery">GALLERY</a></li>
          <li><a href="<?php echo base_url(); ?>loker">LOKER</a></li>
          <li><a href="<?php echo base_url(); ?>pekerja">CARI PEKERJA</a></li>
          <li><a href="<?php echo base_url(); ?>artikel">ARTIKEL</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li class="<?php echo base_url(); ?>active"><a href="kontak"><span class="glyphicon glyphicon-envelope"></span> KONTAK</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- akhir navnya -->

  <br><br><br><br>
  <!-- mulai kontak kaminya -->
  <div class="container contact-form"> 
    <form action="<?php echo base_url('kontak/send'); ?>" method="post" class="" enctype="multipart/form-data">
    
      <h3>Kirimkan Kami Pesan</h3>
      <!-- Pesan sudah terkirim -->
      <?php if($this->session->flashdata('notif')): ?>
        <div class="alert alert-success" role="alert">
          <?php echo $this->session->flashdata('notif'); ?>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="Nama anda *" required/>
          </div>
          <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Email anda *" required/>
          </div>
          <div class="form-group">
            <input type="text" name="subject" class="form-control" placeholder="Subject *" required/>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <textarea name="message" class="form-control" placeholder="Pesan anda *" style="width: 100%; height: 150px;" required></textarea>
          </div>
          <div class="form-group">
            <input type="submit" name="Submit" class="btnContact" value="Kirim Pesan" />
          </div>
        </div>
      </div>
    </form>
  </div>
  <!-- akhir kontak kaminya -->

  <!-- mulai alamat kantornya -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="card text-center">
          <h3>Kantor Pusat</h3>
          <p>
            Jalan Al Badriah 1 Blok C33 NO 4 Pondok Gede Permai, Jatiasih, Bekasi Selatan.
          </p>
        </div>
      </div>
      <div class="col-xs-12 col-md-6">
        <div class="card">
          <h3>Kantor Cabang C38</h3>
          <p>
            Jalan Sunan Bonang Blok C 38 NO 3-5 Pondok Gede Permai, Jatiasih, Bekasi Selatan.
          </p>
        </div>
      </div>
      <div class="col-xs-12 col-md-6">
        <div class="card">
          <h3>Kantor Cabang C40A</h3>
          <p>
            Jalan Sunan Drajat Blok C 40A NO 20 Pondok Gede Permai, Jatiasih, Bekasi Selatan.
          </p>
        </div>
      </div>
      <div class="col-xs-12 col-md-6">
        <div class="card">
          <h3>Kantor Cabang C40B</h3>
          <p>
            Jalan Sunan Drajat Blok C40B NO 19 Pondok Gede Permai, Jatiasih, Bekasi Selatan.
          </p>
        </div>
      </div>
      <div class="col-xs-12 col-md-6">
        <div class="card">
          <h3>Kantor Cabang KCB-001</h3>
          <p>
            Jalan Merpati 1 Blok B9 NO 26 Pondok Gede Permai, Jatiasih, Bekasi Selatan.
          </p>
        </div>
      </div>
    </div>
  </div> 
  <!-- akhir alamat kantornya -->

  <!-- mulai footernya -->
  <footer class="container-fluid">
    <div class="row">
      <div class="col-xs-12">
        <p><span style="color: #93F765" class="glyphicon glyphicon-earphone"></span> (021) 82401313 - (021) 82428499</p>
        <p><span style="color: #93F765" class="glyphicon glyphicon-envelope"></span> lpkpancarankasih2006@gmail.com</p>
        <p><a target="_blank" href="https://www.facebook.com/adminpk20anni/"> <button style="border-radius: 8px;"><i style="color: blue" class="fab fa-facebook-f"></i> LPK Pancaran Kasih Bekasi</button></a></p>
      </div>
    </div>
    <hr>
    <div class="row text-center" style="padding-top: 10px;">
      <div class="col-xs-12">
        © 2019 LPK Pancaran Kasih - All right reserved.
      </div>
    </div>
  </footer>
  <!-- akhir footernya -->
  
</body>
</html>