<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Detail SDM | LPK Pancaran Kasih</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php include 'template/header.php'; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include 'template/navside.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php foreach($hr->result() as $row): ?>
    <?php endforeach; ?>
    <section class="content-header">
      <h1>
        <?php echo $row->nm_lengkap; ?>
        <small>view information</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Data SDM</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
                <div class="col-md-3 col-sm-12">
                    <img src="<?php echo base_url('uploads'); ?>/<?php echo $row->foto; ?>" alt="" class="img img-thumbnail">
                    <div align="center">
                    <a href="<?php echo base_url('admin/human_resource'); ?>" class="btn btn-primary"><i class="fa fa-arrow-circle-left"></i></a>    
                    <a href="<?php echo base_url('admin/edit_human_resource'); ?>/<?php echo $row->id_worker; ?>" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo base_url('admin/delete_human_resource'); ?>/<?php echo $row->id_worker; ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
              <div class="col-md-3 col-sm-12">
                  <table>
                      <tr>
                          <td><b>Nama Lengkap</b></td>
                          <td> : </td>
                          <td><?php echo $row->nm_lengkap; ?></td>
                      </tr>
                      <tr>
                          <td><b>Nama Panggilan</b></td>
                          <td> : </td>
                          <td><?php echo $row->nm_panggilan; ?></td>
                      </tr>
                      <tr>
                          <td><b>Profesi</b></td>
                          <td> : </td>
                          <td><?php echo $row->nm_pekerjaan; ?></td>
                      </tr>
                      <tr>
                          <td><b>Tinggi Badan</b></td>
                          <td> : </td>
                          <td><?php echo $row->tb." Cm"; ?></td>
                      </tr>
                      <tr>
                          <td><b>Berat Badan</b></td>
                          <td> : </td>
                          <td><?php echo $row->bb." Kg"; ?></td>
                      </tr>
                  </table>
              </div>
              <div class="col-md-3 col-sm-12">
                <table>
                      <tr>
                          <td><b>Agama</b></td>
                          <td> : </td>
                          <td><?php echo $row->agama; ?></td>
                      </tr>
                      <tr>
                          <td><b>Pendidikan Terakhir</b></td>
                          <td> : </td>
                          <td><?php echo $row->pendidikan; ?></td>
                      </tr>
                      <tr>
                          <td><b>Memiliki Anak</b></td>
                          <td> : </td>
                          <td><?php echo $row->punya_anak; ?></td>
                      </tr>
                      <tr>
                          <td><b>Asal</b></td>
                          <td> : </td>
                          <td><?php echo $row->asal; ?></td>
                      </tr>
                      <tr>
                          <td><b>Etnis</b></td>
                          <td> : </td>
                          <td><?php echo $row->suku; ?></td>
                      </tr>
                  </table>
              </div>
              <div class="col-md-3 col-sm-12">
              <table>
                      <tr>
                          <td><b>Domisili</b></td>
                          <td> : </td>
                          <td><?php echo $row->domisili; ?></td>
                      </tr>
                      <tr>
                          <td><b>Umur</b></td>
                          <td> : </td>
                          <td><?php echo $row->umur. "years"; ?></td>
                      </tr>
                      <tr>
                          <td><b>Status Perkawinan</b></td>
                          <td> : </td>
                          <td><?php echo $row->status_kawin; ?></td>
                      </tr>
                      <tr>
                          <td><b>Status</b></td>
                          <td> : </td>
                          <td><?php echo $row->status; ?></td>
                      </tr>
                      <tr>
                          <td><b>Salary</b></td>
                          <td> : </td>
                          <td><?php echo "IDR ".number_format($row->salary,2); ?></td>
                      </tr>
                  </table>
              </div>
              <div class="col-md-3 col-sm-12">
                  <div class="form-group">
                      <label for="">Keahlian : </label>
                      <p><?php echo $row->keahlian; ?></p>
                  </div>
              </div>
              <div class="col-md-3 col-sm-12">
              <div class="form-group">
                      <label for="">Pengalaman : </label>
                      <p><?php echo $row->pengalaman; ?></p>
                  </div>
              </div>
              <div class="col-md-3"></div>
              <div class="col-md-3"></div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php include 'template/footer.php'; ?>

  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/backend'); ?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/backend'); ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/backend'); ?>/dist/js/adminlte.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>