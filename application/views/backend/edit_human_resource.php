<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Edit informasi SDM | LPK Pancaran Kasih</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/dist/css/skins/skin-blue.min.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php include 'template/header.php'; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include 'template/navside.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        SDM
        <small>edit</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">edit informasi sdm</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form edit informasi SDM</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php foreach($hr->result() as $hmn_res): ?>
                <?php endforeach; ?>
                <?php if($this->session->flashdata('error')): ?>
                <div class="alert alert-warning" role="alert">
                    <?php echo $this->session->flashdata('error') ?>
                </div>
                <?php endif; ?>
                <form action="<?php echo base_url('admin/edit_human_resource'); ?>/<?php echo $hmn_res->id_worker; ?>" method="post" enctype="multipart/form-data">
                <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                        <label for="">Nama Lengkap</label>
                        <input type="text" class="form-control" name="nm_lengkap" value="<?php echo $hmn_res->nm_lengkap; ?>" placeholder="Full Name">
                    </div>
                    <div class="form-group">
                        <label for="">Nama Panggilan</label>
                        <input type="text" class="form-control" name="nm_panggilan" value="<?php echo $hmn_res->nm_panggilan; ?>" placeholder="Nickname">
                    </div>
                    <div class="form-group">
                        <label for="">Jenis Kelamin</label>
                        <select name="jns_kelamin" class="form-control">
                            <option value="<?php echo $hmn_res->jns_kelamin; ?>"><?php echo $hmn_res->jns_kelamin; ?></option>
                            <option value="pria">Man</option>
                            <option value="wanita">Woman</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Profesi</label>
                        <select name="profesi" class="form-control">
                            <option value="<?php echo $hmn_res->profesi; ?>"><?php echo $hmn_res->nm_pekerjaan; ?></option>
                            <?php foreach($profession->result() as $prof): ?>
                                <option value="<?php echo $prof->id_pekerjaan; ?>"><?php echo $prof->nm_pekerjaan; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Tinggi Badan</label>
                        <input type="text" class="form-control" name="tb" value="<?php echo $hmn_res->tb; ?>" placeholder="Height in Centimeters">
                    </div>
                    <div class="form-group">
                        <label for="">Berat Badan</label>
                        <input type="text" class="form-control" name="bb" value="<?php echo $hmn_res->bb; ?>" placeholder="Weight in Kilograms">
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                        <label for="">Agama</label>
                        <select name="agama" class="form-control">
                            <option value="<?php echo $hmn_res->agama; ?>"><?php echo $hmn_res->agama; ?></option>
                            <option value="islam">Islam</option>
                            <option value="katholik">Katholik</option>
                            <option value="protestan">Protestan</option>
                            <option value="hindu">Hindu</option>
                            <option value="budha">Budha</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Pendidikan Terakhir</label>
                        <select name="pendidikan" class="form-control">
                            <option value="<?php echo $hmn_res->pendidikan; ?>"><?php echo $hmn_res->pendidikan; ?></option>
                            <option value="diploma">Diploma</option>
                            <option value="sarjana">Sarjana</option>
                            <option value="sd">SD</option>
                            <option value="slta">SLTA</option>
                            <option value="sltp">SLTP</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Memiliki Anak</label>
                        <input type="number" class="form-control" name="punya_anak" value="<?php echo $hmn_res->punya_anak; ?>" placeholder="Have children">
                    </div>
                    <div class="form-group">
                        <label for="">Asal</label>
                        <input type="text" class="form-control" name="asal" value="<?php echo $hmn_res->asal; ?>" placeholder="Come From">
                    </div>
                    <div class="form-group">
                        <label for="">Etnis</label>
                        <select name="suku" class="form-control">
                            <option value="<?php echo $hmn_res->suku; ?>"><?php echo $hmn_res->suku; ?></option>
                            <?php foreach($ethnic->result() as $ethnic): ?>
                                <option value="<?php echo $ethnic->id_suku; ?>"><?php echo $ethnic->nm_suku; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                        <label for="">Domisili</label>
                        <input type="text" class="form-control" name="domisili" value="<?php echo $hmn_res->domisili; ?>" placeholder="Domicile">
                    </div>
                    <div class="form-group">
                        <label for="">Umur</label>
                        <input type="number" class="form-control" name="umur" value="<?php echo $hmn_res->umur; ?>" placeholder="Age">
                    </div>
                    <div class="form-group">
                        <label for="">Status Perkawinan</label>
                        <select name="status_kawin" class="form-control">
                            <option value="<?php echo $hmn_res->status_kawin; ?>"><?php echo $hmn_res->status_kawin; ?></option>
                            <option value="married">Married</option>
                            <option value="single">Single</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" class="form-control">
                            <option value="<?php echo $hmn_res->status; ?>"><?php echo $hmn_res->status; ?></option>
                            <?php foreach($status->result() as $status): ?>
                                <option value="<?php echo $status->id_status; ?>"><?php echo $status->nm_status; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Foto</label>
                        <input type="file" class="form-control" name="foto">
                        <input type="hidden" name="old_photo" value="<?php echo $hmn_res->foto; ?>">
                        <p><b>Old Photo : </b><?php echo $hmn_res->foto; ?></p>
                    </div>
                    <div class="form-group">
                        <label for="">Salary</label>
                        <input type="number" class="form-control" name="salary" value="<?php echo $hmn_res->salary; ?>" placeholder="Salary">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="">Keahlian</label>
                        <textarea name="keahlian" id="editor1" cols="30" rows="10" class="form-control" placeholder="Skills"><?php echo $hmn_res->keahlian; ?></textarea>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="">Pengalaman</label>
                        <textarea name="pengalaman" id="editor2" cols="30" rows="10" class="form-control" placeholer="Experience"><?php echo $hmn_res->pengalaman; ?></textarea>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <input type="submit" name="btnSubmit" class="pull-right btn btn-primary" value="Save">
                </div>
              </form>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php include 'template/footer.php'; ?>

  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/backend'); ?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/backend'); ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/backend'); ?>/dist/js/adminlte.min.js"></script>
<!-- CK Editor -->
<script src="<?php echo base_url('assets/backend'); ?>/bower_components/ckeditor/ckeditor.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    CKEDITOR.replace('editor2')
  })
</script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->

</body>
</html>