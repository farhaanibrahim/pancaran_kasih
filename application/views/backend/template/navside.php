<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="<?php echo base_url('admin'); ?>"><i class="fa  fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li><a href="<?php echo base_url('admin/order'); ?>"><i class="fa fa-shopping-cart"></i> <span>Pesanan</span></a></li>
        <li><a href="<?php echo base_url('admin/inbox'); ?>"><i class="fa fa-send"></i> <span>Pesan Masuk</span></a></li>
        <li><a href="<?php echo base_url('admin/rekrutmen'); ?>"><i class="fa fa-send"></i> <span>Rekrutmen</span></a></li>
        <li><a href="<?php echo base_url('admin/article'); ?>"><i class="fa fa-newspaper-o"></i> <span>Artikel</span></a></li>
        <li><a href="<?php echo base_url('admin/human_resource'); ?>"><i class="fa fa-group"></i> <span>SDM</span></a></li>
        <li><a href="<?php echo base_url('admin/gallery'); ?>"><i class="fa fa-image"></i> <span>Galeri</span></a></li>
        <?php if($this->session->userdata('level') == 'super_admin'): ?>
          <li><a href="<?php echo base_url('admin/management_user'); ?>"><i class="fa fa-user"></i> <span>Manajemen User</span></a></li>
          <li><a href="<?php echo base_url('admin/login_log'); ?>"><i class="fa fa-history"></i> <span>Login Log</span></a></li>
        <?php endif; ?>
        <!--
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
        -->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>