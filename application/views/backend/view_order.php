<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>View Order | LPK Pancaran Kasih</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php include 'template/header.php'; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include 'template/navside.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detail Pesanan
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">detail pesanan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Pesanan</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php foreach($order->result() as $row): ?>
                <?php endforeach; ?>
              <div class="col-md-5 col-sm-12">
                <h4>Informasi Pemesanan</h4>
                <table class="table">
                  <tr>
                      <td>Pemesan</td>
                      <td>:</td>
                      <td><?php echo $row->nama; ?></td>
                  </tr>
                  <tr>
                      <td>Alamat</td>
                      <td>:</td>
                      <td><?php echo $row->alamat; ?></td>
                  </tr>
                  <tr>
                      <td>Handphone</td>
                      <td>:</td>
                      <td><?php echo $row->no_hp; ?></td>
                  </tr>
                  <tr>
                      <td>Email</td>
                      <td>:</td>
                      <td><?php echo $row->email; ?></td>
                  </tr>
                  <tr>
                      <td>Keterangan</td>
                      <td>:</td>
                      <td><?php echo $row->keterangan; ?></td>
                  </tr>
              </table>
              </div>
        
                <div class="col-md-2 col-sm-12">
                    <h4>Informasi Tenaga Kerja</h4>
                    <img src="<?php echo base_url('uploads'); ?>/<?php echo $row->foto; ?>" alt="" class="img img-thumbnail">
                    <div class="btn btn-lg btn-primary" style="width: 100%;">
                        IDR <?php echo number_format($row->salary,2); ?>
                    </div>
                </div>

              <div class="col-md-5 col-sm-12">
                  <br><br>
                <table class="table">
                  <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td><?php echo $row->nm_lengkap; ?></td>
                  </tr>
                  <tr>
                      <td>Profesi</td>
                      <td>:</td>
                      <td><?php echo $row->nm_pekerjaan; ?></td>
                  </tr>
                  <tr>
                      <td>Pendidikan Terakhir</td>
                      <td>:</td>
                      <td><?php echo $row->pendidikan; ?></td>
                  </tr>
                  <tr>
                      <td>Domisili</td>
                      <td>:</td>
                      <td><?php echo $row->domisili; ?></td>
                  </tr>
                  <tr>
                      <td>Informasi</td>
                      <td>:</td>
                      <td>
                        <p>Keahlian : </p><br>
                        <?php echo $row->keahlian; ?><br>
                        <p>Pengalaman : </p><br>
                        <?php echo $row->pengalaman; ?>
                      </td>
                  </tr>
              </table>
              </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php include 'template/footer.php'; ?>

  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/backend'); ?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/backend'); ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/backend'); ?>/dist/js/adminlte.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>