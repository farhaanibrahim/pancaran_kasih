<?php

class AdminModel extends CI_Model
{
    public function getHumanResource()
    {
        $this->db->join('t_jns_pekerjaan', 't_worker.profesi = t_jns_pekerjaan.id_pekerjaan');
        $this->db->join('t_status', 't_worker.status = t_status.id_status');
        $this->db->join('t_suku', 't_worker.suku = t_suku.id_suku');
        return $this->db->get('t_worker');
    }

    public function getHumanResource2($limit, $start)
    {   
        $this->db->limit($limit, $start);
        $this->db->join('t_jns_pekerjaan', 't_worker.profesi = t_jns_pekerjaan.id_pekerjaan');
        $this->db->join('t_status', 't_worker.status = t_status.id_status');
        $this->db->join('t_suku', 't_worker.suku = t_suku.id_suku');
        return $this->db->get('t_worker');
    }

    public function getHumanResourceByID($id)
    {
        $this->db->where('id_worker',$id);
        $this->db->join('t_jns_pekerjaan', 't_worker.profesi = t_jns_pekerjaan.id_pekerjaan');
        $this->db->join('t_status', 't_worker.status = t_status.id_status');
        $this->db->join('t_suku', 't_worker.suku = t_suku.id_suku');
        return $this->db->get('t_worker');
    }

    public function getHumanResourceFilter($data)
    {
        $this->db->join('t_jns_pekerjaan', 't_worker.profesi = t_jns_pekerjaan.id_pekerjaan');
        $this->db->join('t_status', 't_worker.status = t_status.id_status');
        $this->db->join('t_suku', 't_worker.suku = t_suku.id_suku');
        $this->db->where($data);
        return $this->db->get('t_worker');
    }

    public function getHumanResourceFilter2($limit, $start,$data)
    {   
        $this->db->limit($limit, $start);
        $this->db->join('t_jns_pekerjaan', 't_worker.profesi = t_jns_pekerjaan.id_pekerjaan');
        $this->db->join('t_status', 't_worker.status = t_status.id_status');
        $this->db->join('t_suku', 't_worker.suku = t_suku.id_suku');
        $this->db->where($data);
        return $this->db->get('t_worker');
    }

    public function getProfession()
    {
        return $this->db->get('t_jns_pekerjaan');
    }

    public function getEthnic()
    {
        return $this->db->get('t_suku');
    }

    public function getStatus()
    {
        return $this->db->get('t_status');
    }

    public function insertHumanResource($data)
    {
        $this->db->insert('t_worker',$data);
    }

    public function editHumanResource($id,$data)
    {
        $this->db->where('id_worker',$id);
        $this->db->set($data);
        $this->db->update('t_worker');
    }

    public function deleteHumanResource($id)
    {
        $this->db->where('id_worker',$id);
        $this->db->delete('t_worker');
    }

    public function getArticle()
    {
        $this->db->order_by('tgl','desc');
        return $this->db->get('t_artikel');
    }

    public function getArticle2($limit, $start)
    {
        $this->db->order_by('tgl','desc');
        $this->db->limit($limit, $start);
        return $this->db->get('t_artikel');
    }

    public function getArticleBySlug($url_slug)
    {
        $this->db->where('url_slug',$url_slug);
        return $this->db->get('t_artikel');
    }

    public function article_per_page()
    {
        $this->db->order_by('tgl','desc');
        $this->db->limit(4);
        return $this->db->get('t_artikel');
    }

    public function insertArticle($data)
    {
        $this->db->insert('t_artikel',$data);
    }

    public function getArticleByID($id)
    {
        $this->db->where('id_artikel',$id);
        return $this->db->get('t_artikel');
    }

    public function editArticle($id,$data)
    {
        $this->db->where('id_artikel',$id);
        $this->db->set($data);
        $this->db->update('t_artikel');
    }

    public function deleteArticle($id)
    {
        $this->db->where('id_artikel',$id);
        $this->db->delete('t_artikel');
    }

    public function send_app($data)
    {
        $this->db->insert('t_recruitment',$data);
    }

    public function getRecruitment()
    {
        return $this->db->get('t_recruitment');
    }

    public function getRecruitmentByID($id)
    {
        $this->db->where('id_rec',$id);
        return $this->db->get('t_recruitment');
    }

    public function deleteRecruitment($id)
    {
        $this->db->where('id_rec',$id);
        $this->db->delete('t_recruitment');
    }

    public function sendMessage($data)
    {
        $this->db->insert('t_contact',$data);
    }

    public function getInbox()
    {
        $this->db->where('status !=', 'delete');
        $this->db->order_by('date_of_letter','desc');
        return $this->db->get('t_contact');
    }

    public function getInboxDeleted()
    {
        $this->db->where('status =', 'delete');
        $this->db->order_by('date_of_letter','desc');
        return $this->db->get('t_contact');
    }

    public function getStarredInbox()
    {
        $this->db->where('mark', '1');
        $this->db->where('status !=', 'delete');
        $this->db->order_by('date_of_letter','desc');
        return $this->db->get('t_contact');
    }

    public function deleteInbox($id,$update)
    {
        $this->db->where('id_contact',$id);
        $this->db->set($update);
        $this->db->update('t_contact');
    }

    public function deleteInboxPermanently($id)
    {
        $this->db->where('id_contact',$id);
        $this->db->delete('t_contact');
    }

    public function getSentMessage($limit, $start)
    {
        $this->db->where('status =', 'sent');
        $this->db->order_by('date_of_letter','desc');
        $this->db->limit($limit, $start);
        return $this->db->get('t_sent');
    }

    public function getDraftMessage($limit, $start)
    {
        $this->db->where('status =', 'draft');
        $this->db->order_by('date_of_letter','desc');
        $this->db->limit($limit, $start);
        return $this->db->get('t_sent');
    }

    public function getInboxByID($id)
    {
        $this->db->where('id_contact',$id);
        return $this->db->get('t_contact');
    }

    public function mark_the_message($id,$mark)
    {
        $this->db->where('id_contact',$id);
        $this->db->set($mark);
        $this->db->update('t_contact');
    }

    public function set_message_to_read($id,$status)
    {
        $this->db->where('id_contact',$id);
        $this->db->set($status);
        $this->db->update('t_contact');
    }

    public function getUnreadMessage()
    {
        $this->db->where('status','unread');
        return $this->db->get('t_contact');
    }

    public function getOrder()
    {
        $this->db->order_by('date_of_order','asc');
        return $this->db->get('t_order');
    }

    public function getOrderByID($id)
    {
        $this->db->where('id_order',$id);
        $this->db->join('t_worker','t_order.id_worker = t_worker.id_worker');
        $this->db->join('t_jns_pekerjaan', 't_worker.profesi = t_jns_pekerjaan.id_pekerjaan');
        return $this->db->get('t_order');
    }

    public function delete_order($id)
    {
        $this->db->where('id_order',$id);
        $this->db->delete('t_order');
    }

    public function send_order($data)
    {
        $this->db->insert('t_order',$data);
    }

    public function getUserAdmin()
    {
        return $this->db->get('t_admin');
    }

    public function getUserAdminByID($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('t_admin');
    }

    public function create_user($data)
    {
        $this->db->insert('t_admin',$data);
    }

    public function edit_user($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->set($data);
        $this->db->update('t_admin');
    }

    public function delete_user($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('t_admin');
    }

    public function get_login_log()
    {
        return $this->db->get('t_login_log');
    }

    public function getGallery()
    {
        $this->db->order_by('id_gallery','desc');
        return $this->db->get('t_gallery');
    }

    public function get_gallery_limit($limit)
    {
        $this->db->limit($limit);
        $this->db->order_by('id_gallery','desc');
        return $this->db->get('t_gallery');
    }

    public function insert_gallery($data)
    {
        $this->db->insert('t_gallery',$data);
    }

    public function get_gallery_by_id($id_gallery)
    {
        $this->db->where('id_gallery',$id_gallery);
        return $this->db->get('t_gallery');
    }

    public function delete_photo($id_gallery)
    {
        $this->db->where('id_gallery',$id_gallery);
        $this->db->delete('t_gallery');
    }

    public function update_gallery($id_gallery,$data)
    {
        $this->db->where('id_gallery',$id_gallery);
        $this->db->set($data);
        $this->db->update('t_gallery');
    }
}
