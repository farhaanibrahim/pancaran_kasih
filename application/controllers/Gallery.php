<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller 
{
    public function __construct()
	{
		parent::__construct();
		$this->load->model('AdminModel');
	}

	
	public function index()
	{
		//$data['article'] = $this->AdminModel->article_per_page();
		$data['gallery'] = $this->AdminModel->getGallery();
		$this->load->view('frontend/gallery',$data);
	}
}
