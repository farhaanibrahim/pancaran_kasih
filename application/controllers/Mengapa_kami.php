<?php

class Mengapa_kami extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $data['article'] = $this->AdminModel->article_per_page();
        $this->load->view('frontend/mengapa_kami',$data);
    }
}
