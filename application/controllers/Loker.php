<?php
class Loker extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel');
    }

    public function index()
    {
        if (isset($_POST['btnSubmit'])) {

            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240;

            $this->load->library('upload', $config);

            //Upload lampiran
            if ( ! $this->upload->do_upload('lampiran'))
            {
                $this->session->set_flashdata('error_lampiran', 'File Upload Ijazah / Buku nikah asli tidak ter-upload');

                redirect(base_url('loker'));
            }
            else
            {
                $lampiran = $this->upload->data('file_name');
            }

            //Uplaod foto KTP
            if ( ! $this->upload->do_upload('foto_ktp'))
            {
                $this->session->set_flashdata('error_foto_ktp', 'File Foto KTP tidak ter-upload');

                redirect(base_url('loker'));
            }
            else
            {
                $foto_ktp = $this->upload->data('file_name');
            }

            $data = array(
                'id_rec'=>'',
                'nm_lengkap'=>$this->input->post('nm_lengkap'),
                'tgl_lahir'=>$this->input->post('tgl_lahir'),
                'jns_klmn'=>$this->input->post('jns_klmn'),
                'alamat'=>$this->input->post('alamat'),
                'no_hp'=>$this->input->post('no_hp'),
                'no_hp_ortu1'=>$this->input->post('no_hp_ortu1'),
                'no_hp_ortu2'=>$this->input->post('no_hp_ortu2'),
                'email'=>$this->input->post('email'),
                'tgl'=>date('Ymd'),
                'lampiran'=>$lampiran,
                'foto_ktp'=>$foto_ktp
            );

            $this->AdminModel->send_app($data);
            $this->session->set_flashdata('sent','Data berhasil terkirim, silahkan menunggu pesan konfirmasi dari Admin LPK Pancaran Kasih via SMS / Email');
            redirect(base_url('loker'));
        } else {
            $data['article'] = $this->AdminModel->article_per_page();
            $this->load->view('frontend/loker',$data);
        }
    }
}
