<?php

class Artikel extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel');
        $this->load->library('pagination');
    }

    public function index()
    {
        //$data['article2'] = $this->AdminModel->article_per_page();

        $limit_per_page = 5;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->AdminModel->getArticle()->num_rows();
        
        $data['article'] = $this->AdminModel->getArticle2($limit_per_page,$start_index);

        $config['base_url'] = base_url('artikel/index');
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = 3;

        //Konfigurasi pagination
        $config['query_string_segment'] = 'start';
 
        $config['full_tag_open'] = '<nav><ul class="pagination" style="margin-top:0px">';
        $config['full_tag_close'] = '</ul></nav>';
        
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $this->load->view('frontend/artikel',$data);
    }

    public function detail($url_slug)
    {
        $data['article'] = $this->AdminModel->article_per_page();
        $data['detail'] = $this->AdminModel->getArticleBySlug($url_slug);
        $this->load->view('frontend/singlepost',$data);
    }
}
