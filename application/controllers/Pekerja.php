<?php

class Pekerja extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel');
        $this->load->library('pagination');
    }

    public function index()
    {
        //$data['article'] = $this->AdminModel->article_per_page();
        $data['jns_pekerjaan'] = $this->AdminModel->getProfession();
        $data['ethnic'] = $this->AdminModel->getEthnic();
        $data['status'] = $this->AdminModel->getStatus();
            
        $limit_per_page = 8;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->AdminModel->getHumanResource()->num_rows();
        
        $data['sdm'] = $this->AdminModel->getHumanResource2($limit_per_page,$start_index);

        $config['base_url'] = base_url('pekerja/index');
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = 3;
        
        //Konfigurasi pagination style
        $config['query_string_segment'] = 'start';
 
        $config['full_tag_open'] = '<nav><ul class="pagination" style="margin-top:0px">';
        $config['full_tag_close'] = '</ul></nav>';
        
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $this->load->view('frontend/caripekerja',$data);
    }

    public function order($id)
    {
        if (isset($_POST['btnSubmit'])) {
            $data = array(
                'id_order'=>'',
                'id_worker'=>$this->input->post('id_worker'),
                'nama'=>$this->input->post('nama'),
                'alamat'=>$this->input->post('alamat'),
                'no_hp'=>$this->input->post('no_hp'),
                'email'=>$this->input->post('email'),
                'keterangan'=>$this->input->post('keterangan'),
                'date_of_order'=>date('Ymd')
            );

            $this->AdminModel->send_order($data);
            $this->session->set_flashdata('sent','Pesanan berhasil terkirim, silahkan menunggu pesan konfirmasi dari Admin LPK Pancaran Kasih via SMS / Email');
            redirect("pekerja/order/".$id);
        } else {
            $data['article'] = $this->AdminModel->article_per_page();
            $data['sdm'] = $this->AdminModel->getHumanResourceByID($id);
            
            $this->load->view('frontend/profile',$data);
        }
    }

    public function filter()
    {
        $set = array();
        if ($_POST['jns_pekerjaan'] !== "-") {
            $set += array('t_worker.profesi'=>$_POST['jns_pekerjaan']);
        } 
        
        if($_POST['pendidikan'] !== "-"){
            $set += array('t_worker.pendidikan'=>$_POST['pendidikan']);
        } 
        
        if($_POST['status'] !== "-"){
            $set += array('t_worker.status'=>$_POST['status']);
        } 
        
        if($_POST['umur'] !== "-"){
            if ($_POST['umur'] == 17) {
                $set += array(
                    't_worker.umur >='=>$_POST['umur'],
                    't_worker.umur <='=>21
                );
            } elseif ($_POST['umur'] == 22) {
                $set = array(
                    't_worker.umur >='=>$_POST['umur'],
                    't_worker.umur <='=>30
                );
            } else if ($_POST['umur'] == 31) {
                $set += array(
                    't_worker.umur >='=>$_POST['umur'],
                    't_worker.umur <='=>50
                );
            } else {
                $set += array('t_worker.umur >='=>$_POST['umur']);
            }
        } 
        
        if($_POST['jns_kelamin'] !== "-"){
            $set += array('t_worker.jns_kelamin'=>$_POST['jns_kelamin']);
        } 
        
        if($_POST['agama'] !== "-"){
            $set += array('t_worker.agama'=>$_POST['agama']);
        } 
        
        if($_POST['suku'] !== "-"){
            $set += array('t_worker.suku'=>$_POST['suku']);
        } 
        
        if($_POST['status_kawin'] !== "-") {
            $set += array('t_worker.status_kawin'=>$_POST['status_kawin']);
        }
        
        //$data['article'] = $this->AdminModel->article_per_page();
        $data['jns_pekerjaan'] = $this->AdminModel->getProfession();
        $data['ethnic'] = $this->AdminModel->getEthnic();
        $data['status'] = $this->AdminModel->getStatus();

        $limit_per_page = 8;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->AdminModel->getHumanResourceFilter($set)->num_rows();
        
        $data['sdm'] = $this->AdminModel->getHumanResourceFilter2($limit_per_page,$start_index,$set);

        $config['base_url'] = base_url('pekerja/filter');
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = 3;

        //Konfigurasi pagination style
        $config['query_string_segment'] = 'start';
 
        $config['full_tag_open'] = '<nav><ul class="pagination" style="margin-top:0px">';
        $config['full_tag_close'] = '</ul></nav>';
        
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $this->load->view('frontend/filter_pekerja',$data);
    }
}
