<?php

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('LoginModel');
        
    }

    public function index()
    {
        $this->load->view('login');
    }

    public function process()
    {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));

        $data = array(
            'username'=>$username,
            'password'=>$password
        );

        $query = $this->LoginModel->getAccountInfo($data);
        if ($query->num_rows() == 1) {
            foreach ($query->result() as $row) {
                if (($username == $row->username) && ($password == $row->password)) {
                    $data_session = array(
                        'id'=>$row->id,
                        'username'=>$row->username,
                        'level'=>$row->level,
                        'status'=>'login'
                    );
                    $this->session->set_userdata($data_session);

                    $log = array(
                        'id_log'=>'',
                        'username'=>$username,
                        'login_time'=>date('Y-m-d H:i:s'),
                        'ip_address'=>$_SERVER['REMOTE_ADDR']
                    );
                    $this->LoginModel->insertLog($log);
                    redirect(base_url('admin'));
                } else {
                    $this->session->set_flashdata('notif','Wrong Username or Password');
                    redirect(base_url('login'));
                }
            }
        } else {
            $this->session->set_flashdata('notif','Wrong Username or Password');
            redirect(base_url('login'));
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('logged_out',"You're logged out");
        redirect(base_url('login'));
    }
}
