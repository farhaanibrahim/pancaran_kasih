<?php

class Kontak extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $this->load->view('frontend/kontak');
    }

    public function send()
    {
        $data = array(
            'id_contact'=>'',
            'name'=>$this->input->post('name'),
            'from'=>$this->input->post('email'),
            'subject'=>$this->input->post('subject'),
            'content'=>$this->input->post('message'),
            'date_of_letter'=>date('Ymd'),
            'mark'=>'0',
            'status'=>'unread'
        );

        $this->AdminModel->sendMessage($data);
        $this->session->set_flashdata('notif','Pesan berhasil dikirim');
        redirect(base_url('kontak'));
    }
}
