<?php
class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel');

        // load Pagination library
        $this->load->library('pagination');

        if ($this->session->userdata('status') !== 'login') {
            $this->session->set_flashdata('logged_out',"You're logged out");
            redirect(base_url('login'));
        }
    }

    public function index()
    {
        $data['messages'] = $this->AdminModel->getUnreadMessage()->num_rows();
        $data['hr'] = $this->AdminModel->getHumanResource()->num_rows();
        $data['orders'] = $this->AdminModel->getOrder()->num_rows();
        $data['articles'] = $this->AdminModel->getArticle()->num_rows();

        $this->load->view('backend/dashboard',$data);
    }

    public function human_resource()
    {
        $data['hr'] = $this->AdminModel->getHumanResource();
        $this->load->view('backend/human_resources',$data);
    }

    public function add_human_resource()
    {
        if (isset($_POST['btnSubmit'])) {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png';
                $config['max_size']             = 10240;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('foto'))
                {       
                    $this->session->set_flashdata('error',$this->upload->display_errors());
                    redirect(base_url('admin/add_human_resource'));
                }
                else
                {
                    $data = array(
                        'id_worker'=>'',
                        'nm_lengkap'=>$this->input->post('nm_lengkap'),
                        'nm_panggilan'=>$this->input->post('nm_panggilan'),
                        'jns_kelamin'=>$this->input->post('jns_kelamin'),
                        'profesi'=>$this->input->post('profesi'),
                        'tb'=>$this->input->post('tb'),
                        'bb'=>$this->input->post('bb'),
                        'agama'=>$this->input->post('agama'),
                        'pendidikan'=>$this->input->post('pendidikan'),
                        'punya_anak'=>$this->input->post('punya_anak'),
                        'asal'=>$this->input->post('asal'),
                        'suku'=>$this->input->post('suku'),
                        'domisili'=>$this->input->post('domisili'),
                        'keahlian'=>$this->input->post('keahlian'),
                        'pengalaman'=>$this->input->post('pengalaman'),
                        'umur'=>$this->input->post('umur'),
                        'status'=>$this->input->post('status'),
                        'status_kawin'=>$this->input->post('status_kawin'),
                        'foto'=>$this->upload->data('file_name'),
                        'salary'=>$this->input->post('salary')
                    );
                    $this->AdminModel->insertHumanResource($data);
                    $this->session->set_flashdata('success','Data was added successfully');
                    redirect(base_url('admin/human_resource'));
                }
        } else {
            $data['profession'] = $this->AdminModel->getProfession();
            $data['ethnic'] = $this->AdminModel->getEthnic();
            $data['status'] = $this->AdminModel->getStatus();
            $this->load->view('backend/add_human_resource',$data);
        }
        
    }

    public function view_human_resource($id)
    {
        $data['hr'] = $this->AdminModel->getHumanResourceByID($id);
        $this->load->view('backend/view_human_resource',$data);
    }

    public function edit_human_resource($id)
    {
        if (isset($_POST['btnSubmit'])) {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png';
                $config['max_size']             = 10240;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('foto'))
                {       
                    $data = array(
                        'nm_lengkap'=>$this->input->post('nm_lengkap'),
                        'nm_panggilan'=>$this->input->post('nm_panggilan'),
                        'jns_kelamin'=>$this->input->post('jns_kelamin'),
                        'profesi'=>$this->input->post('profesi'),
                        'tb'=>$this->input->post('tb'),
                        'bb'=>$this->input->post('bb'),
                        'agama'=>$this->input->post('agama'),
                        'pendidikan'=>$this->input->post('pendidikan'),
                        'punya_anak'=>$this->input->post('punya_anak'),
                        'asal'=>$this->input->post('asal'),
                        'suku'=>$this->input->post('suku'),
                        'domisili'=>$this->input->post('domisili'),
                        'keahlian'=>$this->input->post('keahlian'),
                        'pengalaman'=>$this->input->post('pengalaman'),
                        'umur'=>$this->input->post('umur'),
                        'status'=>$this->input->post('status'),
                        'status_kawin'=>$this->input->post('status_kawin'),
                        'foto'=>$this->input->post('old_photo'),
                        'salary'=>$this->input->post('salary')
                    );
                    $this->AdminModel->editHumanResource($id,$data);
                    $this->session->set_flashdata('success','Data was edited successfully');
                    redirect(base_url('admin/human_resource'));
                }
                else
                {
                    unlink("./uploads/".$this->input->post('old_photo'));
                    $data = array(
                        'nm_lengkap'=>$this->input->post('nm_lengkap'),
                        'nm_panggilan'=>$this->input->post('nm_panggilan'),
                        'profesi'=>$this->input->post('profesi'),
                        'tb'=>$this->input->post('tb'),
                        'bb'=>$this->input->post('bb'),
                        'agama'=>$this->input->post('agama'),
                        'pendidikan'=>$this->input->post('pendidikan'),
                        'punya_anak'=>$this->input->post('punya_anak'),
                        'asal'=>$this->input->post('asal'),
                        'suku'=>$this->input->post('suku'),
                        'domisili'=>$this->input->post('domisili'),
                        'keahlian'=>$this->input->post('keahlian'),
                        'pengalaman'=>$this->input->post('pengalaman'),
                        'umur'=>$this->input->post('umur'),
                        'status'=>$this->input->post('status'),
                        'status_kawin'=>$this->input->post('status_kawin'),
                        'foto'=>$this->upload->data('file_name'),
                        'salary'=>$this->input->post('salary')
                    );
                    $this->AdminModel->editHumanResource($id,$data);
                    $this->session->set_flashdata('success','Data was edited successfully');
                    redirect(base_url('admin/human_resource'));
                }
        } else {
            $data['hr'] = $this->AdminModel->getHumanResourceByID($id);
            $data['profession'] = $this->AdminModel->getProfession();
            $data['ethnic'] = $this->AdminModel->getEthnic();
            $data['status'] = $this->AdminModel->getStatus();

            $this->load->view('backend/edit_human_resource',$data);
        }
        
    }

    public function delete_human_resource($id)
    {
        $data = $this->AdminModel->getHumanResourceByID($id);
        foreach ($data->result() as $row) {
            $foto = $row->foto;
        }
        $this->AdminModel->deleteHumanResource($id);
        unlink("./uploads/".$foto);
        $this->session->set_flashdata('success', 'Data was deleted successfully');
        redirect(base_url('admin/human_resource'));
    }

    public function article()
    {
        $data['article'] = $this->AdminModel->getArticle();
        $this->load->view('backend/article',$data);
    }

    public function add_article()
    {
        if (isset($_POST['btnSubmit'])) {
            $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png';
                $config['max_size']             = 10240;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('foto'))
                {       
                    $this->session->set_flashdata('error',$this->upload->display_errors());
                    redirect(base_url('admin/add_human_resource'));
                }
                else
                {
                    /*
                     * Generate SEO friendly URL
                     */
                    $title = strip_tags($this->input->post('judul'));
                    $titleURL = strtolower(url_title($title));
                    $url_slug = $titleURL;

                    $data = array(
                        'id_artikel'=>'',
                        'url_slug'=>$url_slug,
                        'judul'=>$this->input->post('judul'),
                        'isi'=>$this->input->post('isi'),
                        'tgl'=>date('Ymd'),
                        'foto'=>$this->upload->data('file_name')
                    );
                    $this->AdminModel->insertArticle($data);
                    $this->session->set_flashdata('success','Data was added successfully');
                    redirect(base_url('admin/article'));
                }
        } else {
            $this->load->view('backend/add_article');
        }
        
    }

    public function view_article($id)
    {
        $data['article'] = $this->AdminModel->getArticleByID($id);
        $this->load->view('backend/view_article',$data);
    }

    public function edit_article($id)
    {
        if (isset($_POST['btnSubmit'])) {
            $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png';
                $config['max_size']             = 10240;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('foto'))
                {       
                    $title = strip_tags($this->input->post('judul'));
                    $titleURL = strtolower(url_title($title));
                    $url_slug = $titleURL;

                    $data = array(
                        'url_slug'=>$url_slug,
                        'judul'=>$this->input->post('judul'),
                        'isi'=>$this->input->post('isi'),
                        'foto'=>$this->input->post('old_photo')
                    );
                    $this->AdminModel->editArticle($id,$data);
                    $this->session->set_flashdata('success','Data was edited successfully');
                    redirect(base_url('admin/article'));
                }
                else
                {
                    unlink("./uploads/".$this->input->post('old_photo'));

                    $title = strip_tags($this->input->post('judul'));
                    $titleURL = strtolower(url_title($title));
                    $url_slug = $titleURL;

                    $data = array(
                        'url_slug'=>$url_slug,
                        'judul'=>$this->input->post('judul'),
                        'isi'=>$this->input->post('isi'),
                        'foto'=>$this->upload->data('file_name')
                    );
                    $this->AdminModel->editArticle($id,$data);
                    $this->session->set_flashdata('success','Data was edited successfully');
                    redirect(base_url('admin/article'));
                }
        } else {
            $data['article'] = $this->AdminModel->getArticleByID($id);
            $this->load->view('backend/edit_article',$data);
        }
        
    }

    public function delete_article($id)
    {
        $data = $this->AdminModel->getArticleByID($id);
        foreach ($data->result() as $row) {
            $foto = $row->foto;
        }
        $this->AdminModel->deleteArticle($id);
        unlink("./uploads/".$foto);
        $this->session->set_flashdata('success', 'Data was deleted successfully');
        redirect(base_url('admin/article'));
    }

    public function inbox()
    {   
        $data['recruitment'] = $this->AdminModel->getRecruitment();
        $data['inbox'] = $this->AdminModel->getInbox();
        $data['num_unread_message'] = $this->AdminModel->getUnreadMessage()->num_rows();

        $this->load->view('backend/recruitment',$data);
    }

    public function rekrutmen()
    {
        $data['recruitment'] = $this->AdminModel->getRecruitment();
        $data['num_unread_message'] = $this->AdminModel->getUnreadMessage()->num_rows();

        $this->load->view('backend/recruitment2',$data);
    }

    public function view_recruitment($id)
    {
        $data['recruitment'] = $this->AdminModel->getRecruitmentByID($id);
        $this->load->view('backend/view_recruitment',$data);
    }

    public function delete_recruitment($id)
    {
        $this->AdminModel->deleteRecruitment($id);
        $this->session->set_flashdata('notif','Message was deleted successfully');
        redirect(base_url('admin/recruitment'));
    }

    public function mark_message($id)
    {
        $data = $this->AdminModel->getInboxByID($id);
        foreach ($data->result() as $row) {
            $current_mark = $row->mark;
        }

        if ($current_mark == 0) {
            $set_mark = array('mark'=>'1');
        } else {
            $set_mark = array('mark'=>'0');
        }
        $this->AdminModel->mark_the_message($id,$set_mark);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function view_inbox($id)
    {
        $set_read = array('status'=>'read');
        $this->AdminModel->set_message_to_read($id,$set_read);

        $data['inbox'] = $this->AdminModel->getInboxByID($id);
        $data['num_unread_message'] = $this->AdminModel->getUnreadMessage()->num_rows();
        $this->load->view('backend/view_inbox',$data);
    }

    public function delete_message()
    {
        $update = array('status'=>'delete');

        if (count($_POST['delete_message']) > 0) {
            foreach ($this->input->post('delete_message') as $val) {
                $this->AdminModel->deleteInbox($val,$update);
            }
            $this->session->set_flashdata('notif','Some messages has been deleted!');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->session->set_flashdata('error', "You didn't select anything");
			redirect($_SERVER['HTTP_REFERER']);
        }
        
    }

    public function delete_message_permanent()
    {
        if (count($_POST['delete_message']) > 0) {
            foreach ($this->input->post('delete_message') as $val) {
                $this->AdminModel->deleteInboxPermanently($val);
            }

            $this->session->set_flashdata('notif','Some messages has been deleted permanently!');
            redirect(base_url('admin/deleted_message'));
        } else {
            $this->session->set_flashdata('error', "You didn't select anything");
			redirect(base_url('admin/deleted_message'));
        }
    }

    public function deleted_message()
    {
        $data['recruitment'] = $this->AdminModel->getRecruitment();
        $data['inbox'] = $this->AdminModel->getInboxDeleted();
        $data['num_unread_message'] = $this->AdminModel->getUnreadMessage()->num_rows();

        $this->load->view('backend/deleted_message',$data);
    }

    public function starred_message()
    {
        $data['recruitment'] = $this->AdminModel->getRecruitment();
        $data['inbox'] = $this->AdminModel->getStarredInbox();
        $data['num_unread_message'] = $this->AdminModel->getUnreadMessage()->num_rows();

        $this->load->view('backend/starred_message',$data);
    }

    public function sent_message()
    {
        $limit_per_page = 20;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->AdminModel->getSentMessage($limit_per_page,$start_index)->num_rows();

        $data['recruitment'] = $this->AdminModel->getRecruitment();
        $data['inbox'] = $this->AdminModel->getSentMessage($limit_per_page,$start_index);

        $config['base_url'] = base_url()."admin/recruitment/";
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = 3;
        $config['display_pages'] = FALSE;
        $config['next_link'] = '<i class="fa fa-chevron-right"></i>';
        $config['next_tag_open'] = '<div class="btn btn-default btn-sm">';
        $config['next_tag_close'] = '</div>';
        $config['prev_link'] = '<i class="fa fa-chevron-left"></i>';
        $config['prev_tag_open'] = '<div class="btn btn-default btn-sm">';
        $config['prev_tag_close'] = '</div>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();
        $data['num_unread_message'] = $this->AdminModel->getUnreadMessage()->num_rows();

        $this->load->view('backend/sent_message',$data);
    }

    public function draft_message()
    {
        $limit_per_page = 20;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->AdminModel->getDraftMessage($limit_per_page,$start_index)->num_rows();

        $data['recruitment'] = $this->AdminModel->getRecruitment();
        $data['inbox'] = $this->AdminModel->getDraftMessage($limit_per_page,$start_index);

        $config['base_url'] = base_url()."admin/recruitment/";
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = 3;
        $config['display_pages'] = FALSE;
        $config['next_link'] = '<i class="fa fa-chevron-right"></i>';
        $config['next_tag_open'] = '<div class="btn btn-default btn-sm">';
        $config['next_tag_close'] = '</div>';
        $config['prev_link'] = '<i class="fa fa-chevron-left"></i>';
        $config['prev_tag_open'] = '<div class="btn btn-default btn-sm">';
        $config['prev_tag_close'] = '</div>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();
        $data['num_unread_message'] = $this->AdminModel->getUnreadMessage()->num_rows();

        $this->load->view('backend/draft_message',$data);
    }

    public function order()
    {
        $data['order'] = $this->AdminModel->getOrder();
        $this->load->view('backend/order',$data);
    }

    public function delete_order($id)
    {
        $this->AdminModel->delete_order($id);
        $this->session->set_flashdata('notif', 'Data was deleted successfully ');
        redirect(base_url('admin/order'));
    }

    public function view_order($id)
    {
        $data['order'] = $this->AdminModel->getOrderByID($id);
        $this->load->view('backend/view_order',$data);
    }

    public function management_user()
    {
        $data['user'] = $this->AdminModel->getUserAdmin();
        $this->load->view('backend/management_user',$data);
    }

    public function create_user()
    {
        if (isset($_POST['btnSubmit'])) {
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $re_pass = md5($this->input->post('re_pass'));

            if ($password !== $re_pass) {
                $this->session->set_flashdata('notif','Re-type password doesnt match');
                redirect(base_url('admin/create_user'));
            } else {
                $data = array(
                    'id'=>'',
                    'username'=>$username,
                    'password'=>$password,
                    'level'=>'admin'
                );
                $this->AdminModel->create_user($data);
                $this->session->set_flashdata('notif','User wa created successfully');
                redirect(base_url('admin/management_user'));
            }
            
        } else {
            $this->load->view('backend/create_user');
        }
        
    }

    public function edit_user($id)
    {
        if (isset($_POST['btnSubmit'])) {
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $data = $this->AdminModel->getUserAdminByID($id);
            foreach ($data->result() as $row) {
                $old_username = $row->username;
                $get_pass = $row->password;
            }
            if ($password !== $get_pass) {
                $this->session->set_flashdata('notif',"Password you've entered doesnt match");
                redirect(base_url('admin/edit_user/'.$id));
            } else {
                $set = array('username'=>$username);
                $this->AdminModel->edit_user($id,$set);
                $this->session->set_flashdata('notif',"The ".$old_username." username has been changed to ".$username."");
                redirect(base_url('admin/management_user'));
            }
            
        } elseif (isset($_POST['btnChgPass'])) {
            $old_password = md5($this->input->post('old_password'));
            $new_password  = md5($this->input->post('new_password'));
            $data = $this->AdminModel->getUserAdminByID($id);
            foreach ($data->result() as $row) {
                $username = $row->username;
                $get_pass = $row->password;
            }
            if ($old_password !== $get_pass) {
                $this->session->set_flashdata('notif',"Te old password you've entered doesnt match");
                redirect(base_url('admin/edit_user/'.$id));
            } else {
                $set = array('password'=>$new_password);
                $this->AdminModel->edit_user($id,$set);
                $this->session->set_flashdata('notif',"".$username." username password has been changed to");
                redirect(base_url('admin/management_user'));
            }
        } 
        else {
            $data['user'] = $this->AdminModel->getUserAdminByID($id);
            $this->load->view('backend/edit_user',$data);
        }
        
    }

    public function delete_user($id)
    {
        $this->AdminModel->delete_user($id);
        $this->session->set_flashdata('notif','User was deleted successfully');
        redirect(base_url('admin/management_user'));
    }

    public function login_log()
    {
        $data['log'] = $this->AdminModel->get_login_log();
        $this->load->view('backend/login_log',$data);
    }

    public function gallery()
    {
        $data['gallery'] = $this->AdminModel->getGallery();
        $this->load->view('backend/gallery',$data);
    }

    public function add_gallery()
    {
        if (isset($_POST['btnSubmit'])) {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png';
                $config['max_size']             = 10240;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('photo'))
                {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect(base_url('admin/add_gallery'));
                }
                else
                {
                    $data = array(
                        'id_gallery'=>'',
                        'title'=>$this->input->post('title'),
                        'file'=>$this->upload->data('file_name'),
                        'date_upload'=>date('Ymd')
                    );
                    $this->AdminModel->insert_gallery($data);
                    $this->session->set_flashdata('success', 'Data was uploaded successfully');
                    redirect(base_url('admin/gallery'));
                }
        } else {
            $this->load->view('backend/add_gallery');
        }
        
    }

    public function delete_foto($id_gallery)
    {
        $get_info = $this->AdminModel->get_gallery_by_id($id_gallery);
        foreach ($get_info->result() as $row) {
            $file = $row->file;
        }
        unlink("./uploads/".$file);
        $this->AdminModel->delete_photo($id_gallery);

        $this->session->set_flashdata('notif','Photo was deleted successfully');
        redirect(base_url('admin/gallery'));
    }

    public function edit_foto($id_gallery)
    {
        if (isset($_POST['btnSubmit'])) {
            $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png';
                $config['max_size']             = 10240;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('photo'))
                {
                    $data = array(
                        'title'=>$this->input->post('title'),
                        'file'=>$this->input->post('old_photo'),
                    );
                    $this->AdminModel->update_gallery($id_gallery,$data);
                    $this->session->set_flashdata('notif', 'Data was updated successfully');
                    redirect(base_url('admin/gallery'));
                }
                else
                {
                    unlink("./uploads/".$this->input->post('old_photo'));
                    $data = array(
                        'title'=>$this->input->post('title'),
                        'file'=>$this->upload->data('file_name'),
                    );
                    $this->AdminModel->update_gallery($id_gallery,$data);
                    $this->session->set_flashdata('notif', 'Data was updated successfully');
                    redirect(base_url('admin/gallery'));
                }
        } else {
            $data['gallery'] = $this->AdminModel->get_gallery_by_id($id_gallery);
            $this->load->view('backend/edit_gallery',$data);
        }
        
    }
}

