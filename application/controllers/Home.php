<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('AdminModel');
	}

	
	public function index()
	{
		$data['article'] = $this->AdminModel->article_per_page();
		$data['gallery'] = $this->AdminModel->get_gallery_limit(8);
		$this->load->view('frontend/index',$data);
	}
}
