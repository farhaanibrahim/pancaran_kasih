-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2019 at 05:40 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pancaran_kasih`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_admin`
--

CREATE TABLE `t_admin` (
  `id` int(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_admin`
--

INSERT INTO `t_admin` (`id`, `username`, `password`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'super_admin'),
(3, 'farhaanibrahim', '6cfa9279712766d5c79379672021dbfc', 'super_admin'),
(4, 'admin1', 'e00cf25ad42683b3df678c61f42c6bda', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `t_artikel`
--

CREATE TABLE `t_artikel` (
  `id_artikel` int(30) NOT NULL,
  `url_slug` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `tgl` date NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_artikel`
--

INSERT INTO `t_artikel` (`id_artikel`, `url_slug`, `judul`, `isi`, `tgl`, `foto`) VALUES
(2, 'eu-facilisis-sed-odio-morbi-quis-commodo-odio-aenean-sed', 'eu facilisis sed odio morbi quis commodo odio aenean sed', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada. Ut eu sem integer vitae justo. Turpis egestas integer eget aliquet nibh. Cursus in hac habitasse platea dictumst quisque sagittis purus sit. Sed viverra ipsum nunc aliquet bibendum enim facilisis gravida neque. Donec et odio pellentesque diam volutpat commodo sed. Venenatis lectus magna fringilla urna. Sagittis eu volutpat odio facilisis mauris sit amet. Purus sit amet luctus venenatis lectus. Ac ut consequat semper viverra nam libero justo laoreet. Sit amet dictum sit amet. Faucibus in ornare quam viverra orci sagittis eu volutpat. Nam libero justo laoreet sit amet cursus sit.</p>\r\n\r\n<p>Sit amet nulla facilisi morbi tempus iaculis urna id. Nascetur ridiculus mus mauris vitae ultricies leo integer. Nisl condimentum id venenatis a condimentum vitae. Eget gravida cum sociis natoque penatibus et magnis. Urna nunc id cursus metus aliquam eleifend. Mattis nunc sed blandit libero. Sit amet cursus sit amet dictum sit. Hendrerit gravida rutrum quisque non tellus. Massa enim nec dui nunc mattis enim ut tellus elementum. Hac habitasse platea dictumst vestibulum rhoncus.</p>\r\n\r\n<p>Consectetur a erat nam at lectus urna. In massa tempor nec feugiat nisl pretium fusce id velit. Risus nec feugiat in fermentum. Orci nulla pellentesque dignissim enim sit amet venenatis urna. Duis convallis convallis tellus id interdum. Molestie at elementum eu facilisis sed odio morbi quis commodo. Quis hendrerit dolor magna eget est lorem ipsum dolor sit. Nunc pulvinar sapien et ligula ullamcorper malesuada proin libero. Nisl rhoncus mattis rhoncus urna neque viverra justo. Aliquet bibendum enim facilisis gravida neque convallis a cras semper. Egestas congue quisque egestas diam in arcu. Tellus molestie nunc non blandit massa enim nec dui nunc. Amet justo donec enim diam vulputate. Ultrices dui sapien eget mi proin sed libero enim. Euismod elementum nisi quis eleifend quam adipiscing. Netus et malesuada fames ac turpis egestas maecenas pharetra convallis. Integer vitae justo eget magna fermentum iaculis eu non. Cursus mattis molestie a iaculis at. Ipsum dolor sit amet consectetur adipiscing elit pellentesque.</p>\r\n\r\n<p>Sit amet facilisis magna etiam. Id diam maecenas ultricies mi eget mauris pharetra et. Posuere sollicitudin aliquam ultrices sagittis orci a scelerisque. Magna eget est lorem ipsum. Orci dapibus ultrices in iaculis nunc sed. Turpis massa sed elementum tempus egestas sed sed. Gravida quis blandit turpis cursus in hac. Feugiat sed lectus vestibulum mattis ullamcorper velit. Eros in cursus turpis massa. Risus nec feugiat in fermentum posuere. Justo eget magna fermentum iaculis eu non. At auctor urna nunc id cursus metus aliquam eleifend mi. A scelerisque purus semper eget duis at tellus. Lectus arcu bibendum at varius vel pharetra. Tortor vitae purus faucibus ornare. Tristique nulla aliquet enim tortor at. Nec dui nunc mattis enim. Faucibus vitae aliquet nec ullamcorper. Nam at lectus urna duis convallis convallis tellus id. Nulla pellentesque dignissim enim sit amet.</p>\r\n\r\n<p>Duis at consectetur lorem donec massa sapien faucibus et. Fermentum posuere urna nec tincidunt praesent semper feugiat nibh sed. Turpis egestas integer eget aliquet nibh praesent tristique magna. Dictum sit amet justo donec. Suscipit tellus mauris a diam maecenas sed enim. Nibh sit amet commodo nulla facilisi nullam vehicula. Vel turpis nunc eget lorem dolor. Hendrerit dolor magna eget est lorem ipsum dolor sit amet. Amet consectetur adipiscing elit pellentesque habitant. Commodo sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Fermentum posuere urna nec tincidunt praesent semper feugiat nibh sed. Eros in cursus turpis massa tincidunt dui.</p>\r\n\r\n<p>Urna nunc id cursus metus aliquam eleifend mi in. Urna cursus eget nunc scelerisque viverra. Non sodales neque sodales ut etiam sit amet. Urna nunc id cursus metus aliquam eleifend mi in. Sapien faucibus et molestie ac feugiat sed lectus vestibulum. Tellus rutrum tellus pellentesque eu tincidunt tortor aliquam. Blandit libero volutpat sed cras ornare arcu dui. Ullamcorper a lacus vestibulum sed arcu non odio. Eget nunc lobortis mattis aliquam faucibus. Risus pretium quam vulputate dignissim suspendisse in est ante in.</p>\r\n\r\n<p>Egestas quis ipsum suspendisse ultrices gravida dictum fusce ut. Sed lectus vestibulum mattis ullamcorper velit sed. Magna fringilla urna porttitor rhoncus dolor. Suscipit adipiscing bibendum est ultricies integer. Quam id leo in vitae turpis massa sed elementum. Leo in vitae turpis massa sed elementum tempus egestas. Maecenas accumsan lacus vel facilisis volutpat est velit egestas. Ultrices eros in cursus turpis massa tincidunt dui ut. Ut sem nulla pharetra diam sit amet nisl. Elementum nibh tellus molestie nunc non blandit massa enim. Quisque non tellus orci ac auctor augue mauris. In vitae turpis massa sed elementum. Sodales neque sodales ut etiam sit amet nisl purus in. Duis ultricies lacus sed turpis. A iaculis at erat pellentesque adipiscing commodo elit. Placerat orci nulla pellentesque dignissim enim sit. Feugiat sed lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi. Sit amet facilisis magna etiam tempor. Tortor posuere ac ut consequat.</p>\r\n\r\n<p>Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Mauris in aliquam sem fringilla ut morbi tincidunt. Id donec ultrices tincidunt arcu non. Ut sem nulla pharetra diam sit amet nisl suscipit. A diam sollicitudin tempor id eu nisl nunc. Maecenas pharetra convallis posuere morbi leo. Mattis ullamcorper velit sed ullamcorper morbi tincidunt. In nibh mauris cursus mattis molestie. Amet dictum sit amet justo donec enim. Mattis molestie a iaculis at erat.</p>\r\n\r\n<p>Lobortis feugiat vivamus at augue eget arcu dictum varius. Orci sagittis eu volutpat odio. Accumsan in nisl nisi scelerisque. Facilisis gravida neque convallis a cras. Condimentum id venenatis a condimentum vitae sapien pellentesque. Sed sed risus pretium quam vulputate dignissim suspendisse. Neque vitae tempus quam pellentesque nec nam aliquam sem. Cursus vitae congue mauris rhoncus aenean vel elit. Aliquam vestibulum morbi blandit cursus risus. Aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus. Malesuada fames ac turpis egestas sed tempus urna. Hendrerit dolor magna eget est lorem ipsum dolor sit. Sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Orci a scelerisque purus semper eget duis at. Elementum integer enim neque volutpat ac tincidunt vitae. Amet volutpat consequat mauris nunc. Scelerisque felis imperdiet proin fermentum leo vel orci.</p>\r\n\r\n<p>Et ultrices neque ornare aenean euismod elementum nisi. Scelerisque varius morbi enim nunc faucibus a pellentesque sit amet. Et ultrices neque ornare aenean euismod elementum nisi quis. Id diam maecenas ultricies mi eget mauris pharetra et. In cursus turpis massa tincidunt dui. Neque ornare aenean euismod elementum nisi. Erat pellentesque adipiscing commodo elit at. Cras semper auctor neque vitae. Urna duis convallis convallis tellus id interdum velit laoreet. Urna et pharetra pharetra massa. Rhoncus aenean vel elit scelerisque mauris. Pharetra massa massa ultricies mi quis.</p>\r\n', '2018-10-14', '1.jpg'),
(3, 'vitae-auctor-eu-augue-ut-lectus-arcu-bibendum-at-varius', 'vitae auctor eu augue ut lectus arcu bibendum at varius', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris a diam maecenas sed enim. Iaculis nunc sed augue lacus viverra vitae. Tempor orci dapibus ultrices in iaculis nunc. Vitae sapien pellentesque habitant morbi tristique senectus et. Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec. Dictumst vestibulum rhoncus est pellentesque. Eget nunc scelerisque viverra mauris in aliquam. Commodo elit at imperdiet dui accumsan sit amet nulla facilisi. Eget velit aliquet sagittis id. Dui nunc mattis enim ut tellus elementum sagittis vitae et. Sit amet mauris commodo quis imperdiet massa tincidunt. Maecenas pharetra convallis posuere morbi leo urna molestie at. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Faucibus nisl tincidunt eget nullam non nisi. Aenean sed adipiscing diam donec adipiscing.</p>\r\n\r\n<p>Sit amet dictum sit amet justo donec enim. At risus viverra adipiscing at in tellus integer feugiat scelerisque. A pellentesque sit amet porttitor eget dolor. Placerat duis ultricies lacus sed turpis tincidunt id aliquet risus. Id ornare arcu odio ut. Mi sit amet mauris commodo quis. Nam at lectus urna duis convallis. A iaculis at erat pellentesque adipiscing commodo. Eget mi proin sed libero enim sed faucibus. Arcu vitae elementum curabitur vitae nunc sed velit dignissim. Commodo sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec. Lobortis mattis aliquam faucibus purus in massa. Massa id neque aliquam vestibulum morbi. Facilisi etiam dignissim diam quis enim lobortis scelerisque. Lorem mollis aliquam ut porttitor leo a diam sollicitudin. Quam nulla porttitor massa id neque aliquam vestibulum. Nulla malesuada pellentesque elit eget gravida. A erat nam at lectus urna duis.</p>\r\n\r\n<p>Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi. Et ligula ullamcorper malesuada proin. Nulla facilisi morbi tempus iaculis. Eu feugiat pretium nibh ipsum consequat nisl vel pretium lectus. Rhoncus est pellentesque elit ullamcorper dignissim cras. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor elit. Eu facilisis sed odio morbi quis commodo odio aenean. Lobortis mattis aliquam faucibus purus in massa tempor nec feugiat. Interdum velit laoreet id donec. Dignissim enim sit amet venenatis urna cursus. Sit amet massa vitae tortor condimentum lacinia quis vel. Urna condimentum mattis pellentesque id nibh. Commodo elit at imperdiet dui accumsan. In egestas erat imperdiet sed euismod nisi porta lorem mollis. Mollis aliquam ut porttitor leo a diam sollicitudin tempor id. Sed sed risus pretium quam vulputate. Vulputate sapien nec sagittis aliquam. Feugiat nisl pretium fusce id velit ut tortor pretium viverra. Scelerisque fermentum dui faucibus in. Nulla facilisi etiam dignissim diam quis enim lobortis scelerisque fermentum.</p>\r\n\r\n<p>Gravida neque convallis a cras semper. Quisque sagittis purus sit amet volutpat. Et pharetra pharetra massa massa ultricies mi quis. Varius duis at consectetur lorem donec massa sapien faucibus et. Urna cursus eget nunc scelerisque viverra mauris in aliquam. Amet massa vitae tortor condimentum lacinia quis vel eros donec. Parturient montes nascetur ridiculus mus mauris vitae ultricies. Viverra mauris in aliquam sem fringilla ut morbi. Hac habitasse platea dictumst quisque sagittis purus sit amet volutpat. Eu lobortis elementum nibh tellus molestie. Maecenas ultricies mi eget mauris pharetra et ultrices neque. Amet tellus cras adipiscing enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames. Quis viverra nibh cras pulvinar mattis nunc sed. Dignissim diam quis enim lobortis scelerisque. Ultrices vitae auctor eu augue ut. Egestas fringilla phasellus faucibus scelerisque eleifend. Elementum nibh tellus molestie nunc non blandit massa. Volutpat ac tincidunt vitae semper.</p>\r\n\r\n<p>Neque convallis a cras semper. Purus semper eget duis at tellus. Aenean euismod elementum nisi quis eleifend quam adipiscing vitae. Odio facilisis mauris sit amet massa vitae. Proin fermentum leo vel orci porta. Viverra nam libero justo laoreet sit amet cursus sit amet. Ipsum nunc aliquet bibendum enim facilisis. Eget mauris pharetra et ultrices neque. Bibendum est ultricies integer quis auctor elit sed. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Diam maecenas ultricies mi eget mauris pharetra et ultrices. Nisi est sit amet facilisis magna etiam. Elementum sagittis vitae et leo. Consectetur libero id faucibus nisl tincidunt eget nullam non. Morbi non arcu risus quis varius quam quisque id diam. Lectus proin nibh nisl condimentum id venenatis a condimentum.</p>\r\n\r\n<p>Faucibus purus in massa tempor nec. At lectus urna duis convallis convallis tellus. Tincidunt dui ut ornare lectus. Ut consequat semper viverra nam libero justo. Sed velit dignissim sodales ut eu sem integer. Eget velit aliquet sagittis id consectetur purus ut faucibus. Vitae suscipit tellus mauris a diam maecenas. Sed lectus vestibulum mattis ullamcorper. Imperdiet sed euismod nisi porta lorem mollis aliquam. Feugiat vivamus at augue eget arcu dictum varius duis. Venenatis lectus magna fringilla urna. Adipiscing enim eu turpis egestas pretium aenean pharetra.</p>\r\n\r\n<p>Tristique sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula. Vitae proin sagittis nisl rhoncus mattis rhoncus urna. Bibendum neque egestas congue quisque egestas diam in arcu cursus. Dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim. Condimentum lacinia quis vel eros donec ac odio. Scelerisque eleifend donec pretium vulputate sapien nec sagittis aliquam. Sagittis id consectetur purus ut. Dui accumsan sit amet nulla facilisi morbi tempus iaculis. Nec tincidunt praesent semper feugiat nibh sed. Urna porttitor rhoncus dolor purus non enim praesent. Tellus id interdum velit laoreet id donec. Vitae purus faucibus ornare suspendisse sed. Sit amet mattis vulputate enim nulla aliquet porttitor lacus. Volutpat blandit aliquam etiam erat. Vel fringilla est ullamcorper eget nulla facilisi etiam dignissim.</p>\r\n\r\n<p>Turpis tincidunt id aliquet risus feugiat in ante. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Nisl purus in mollis nunc sed id semper risus. Turpis massa tincidunt dui ut ornare lectus. Commodo ullamcorper a lacus vestibulum sed. Ac ut consequat semper viverra nam libero justo laoreet sit. Consequat nisl vel pretium lectus quam. Nunc aliquet bibendum enim facilisis gravida. Volutpat blandit aliquam etiam erat velit scelerisque in dictum non. Nisi vitae suscipit tellus mauris a. Platea dictumst vestibulum rhoncus est. Eget nunc lobortis mattis aliquam. Ultrices neque ornare aenean euismod elementum. Risus nullam eget felis eget nunc lobortis mattis. Euismod elementum nisi quis eleifend quam. Morbi tempus iaculis urna id volutpat. Nunc pulvinar sapien et ligula ullamcorper malesuada. Sed viverra ipsum nunc aliquet.</p>\r\n\r\n<p>Bibendum arcu vitae elementum curabitur vitae nunc. Facilisis magna etiam tempor orci eu lobortis elementum nibh. Risus ultricies tristique nulla aliquet. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus. At erat pellentesque adipiscing commodo elit at imperdiet dui accumsan. In massa tempor nec feugiat nisl pretium fusce. Vitae tortor condimentum lacinia quis vel eros donec ac. Felis bibendum ut tristique et egestas quis ipsum. Vestibulum lectus mauris ultrices eros in cursus turpis massa tincidunt. Vitae semper quis lectus nulla. Faucibus ornare suspendisse sed nisi lacus. Libero nunc consequat interdum varius sit amet. Scelerisque viverra mauris in aliquam sem fringilla ut. Laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Curabitur vitae nunc sed velit.</p>\r\n\r\n<p>Pharetra vel turpis nunc eget lorem. Fermentum odio eu feugiat pretium nibh. Quam pellentesque nec nam aliquam sem et. Neque convallis a cras semper. Massa sapien faucibus et molestie ac feugiat. Quis eleifend quam adipiscing vitae proin sagittis. Pellentesque sit amet porttitor eget dolor morbi non. Accumsan tortor posuere ac ut. Rhoncus urna neque viverra justo. Tempor orci dapibus ultrices in iaculis nunc sed. Leo a diam sollicitudin tempor id eu. Arcu felis bibendum ut tristique et egestas quis ipsum. Viverra tellus in hac habitasse platea.</p>\r\n', '2018-10-14', '2.jpg'),
(4, 'nisl-nunc-mi-ipsum-faucibus-vitae-aliquet-nec-ullamcorper-sit', 'nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper sit', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque egestas congue quisque egestas diam in. Bibendum est ultricies integer quis auctor elit sed. Ut etiam sit amet nisl purus in mollis nunc. Metus dictum at tempor commodo ullamcorper. Urna cursus eget nunc scelerisque viverra mauris in aliquam. Aenean pharetra magna ac placerat. Urna duis convallis convallis tellus id. Hac habitasse platea dictumst vestibulum rhoncus est. Suspendisse potenti nullam ac tortor vitae purus faucibus ornare suspendisse.</p>\r\n\r\n<p>Nulla porttitor massa id neque aliquam vestibulum morbi. Sed faucibus turpis in eu. Molestie nunc non blandit massa enim nec. Libero volutpat sed cras ornare arcu dui vivamus. Tempus quam pellentesque nec nam. Tellus pellentesque eu tincidunt tortor aliquam. Consequat mauris nunc congue nisi. Eu ultrices vitae auctor eu augue ut lectus arcu bibendum. Enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit. Nisi scelerisque eu ultrices vitae auctor eu. Lacus vel facilisis volutpat est velit egestas dui id ornare.</p>\r\n\r\n<p>In massa tempor nec feugiat nisl pretium fusce id. Elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique. Ultrices sagittis orci a scelerisque purus semper. Adipiscing elit pellentesque habitant morbi. Tortor aliquam nulla facilisi cras fermentum odio eu feugiat. Tortor pretium viverra suspendisse potenti nullam ac tortor vitae. Sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae nunc. Vel orci porta non pulvinar neque laoreet. Ac tortor dignissim convallis aenean. Sem integer vitae justo eget magna fermentum iaculis eu. Fusce ut placerat orci nulla pellentesque dignissim enim. Purus in mollis nunc sed.</p>\r\n\r\n<p>A diam maecenas sed enim. Cras sed felis eget velit aliquet sagittis id consectetur. Id donec ultrices tincidunt arcu. Ultrices in iaculis nunc sed augue. Consectetur a erat nam at lectus urna duis convallis. Neque sodales ut etiam sit amet nisl purus in mollis. Senectus et netus et malesuada. Sit amet risus nullam eget felis. Consectetur libero id faucibus nisl. Lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor. Pulvinar etiam non quam lacus suspendisse faucibus interdum. Convallis posuere morbi leo urna molestie. Ut venenatis tellus in metus vulputate eu scelerisque. Platea dictumst vestibulum rhoncus est. Nulla aliquet enim tortor at. Tincidunt praesent semper feugiat nibh sed pulvinar. Eget sit amet tellus cras adipiscing enim. Senectus et netus et malesuada fames ac turpis egestas. Egestas sed sed risus pretium.</p>\r\n\r\n<p>Ultrices in iaculis nunc sed augue lacus viverra. Convallis a cras semper auctor. Lacus sed viverra tellus in hac habitasse platea dictumst. Vitae tortor condimentum lacinia quis vel eros donec ac. Id porta nibh venenatis cras sed felis eget. Ornare lectus sit amet est placerat in egestas erat. Posuere sollicitudin aliquam ultrices sagittis orci a. Quam vulputate dignissim suspendisse in est ante in nibh. Amet nisl suscipit adipiscing bibendum est ultricies. Egestas quis ipsum suspendisse ultrices gravida dictum. Sem integer vitae justo eget. Fusce ut placerat orci nulla pellentesque dignissim enim sit amet. Malesuada fames ac turpis egestas maecenas pharetra. Dolor sit amet consectetur adipiscing elit. Lectus arcu bibendum at varius vel pharetra vel. Volutpat maecenas volutpat blandit aliquam etiam erat. Nec nam aliquam sem et tortor consequat.</p>\r\n\r\n<p>Iaculis at erat pellentesque adipiscing commodo elit at. Velit dignissim sodales ut eu sem. Tortor at risus viverra adipiscing at in tellus. Malesuada fames ac turpis egestas sed tempus urna et pharetra. Fames ac turpis egestas integer eget aliquet. Diam phasellus vestibulum lorem sed risus ultricies. Elementum tempus egestas sed sed risus pretium quam. Massa tincidunt dui ut ornare lectus sit amet est placerat. Lacus laoreet non curabitur gravida arcu ac tortor dignissim convallis. Vitae suscipit tellus mauris a diam maecenas. Quis eleifend quam adipiscing vitae proin sagittis. Tincidunt tortor aliquam nulla facilisi. Tristique magna sit amet purus gravida quis blandit turpis. Dis parturient montes nascetur ridiculus mus mauris. Laoreet sit amet cursus sit amet dictum sit amet justo. Elementum integer enim neque volutpat. Sem integer vitae justo eget magna fermentum iaculis eu non. Risus ultricies tristique nulla aliquet enim tortor at auctor. Congue nisi vitae suscipit tellus mauris a diam maecenas sed. Et ligula ullamcorper malesuada proin.</p>\r\n\r\n<p>Fermentum iaculis eu non diam phasellus vestibulum lorem sed. Varius morbi enim nunc faucibus. Tellus id interdum velit laoreet id donec ultrices. Senectus et netus et malesuada. Nullam eget felis eget nunc lobortis mattis aliquam. In eu mi bibendum neque egestas congue quisque egestas diam. Diam sit amet nisl suscipit adipiscing. Viverra ipsum nunc aliquet bibendum enim facilisis gravida neque. Pretium vulputate sapien nec sagittis aliquam malesuada bibendum. Massa id neque aliquam vestibulum morbi blandit cursus risus. Est ullamcorper eget nulla facilisi etiam dignissim diam quis. Facilisi nullam vehicula ipsum a arcu cursus. In fermentum et sollicitudin ac orci phasellus egestas tellus. Purus sit amet luctus venenatis. Commodo elit at imperdiet dui accumsan sit amet nulla. Tincidunt eget nullam non nisi est sit amet. Diam sit amet nisl suscipit adipiscing. Sed nisi lacus sed viverra tellus in.</p>\r\n\r\n<p>Ultrices dui sapien eget mi. Lorem donec massa sapien faucibus. Proin gravida hendrerit lectus a. Volutpat est velit egestas dui. Rhoncus aenean vel elit scelerisque mauris. Et malesuada fames ac turpis. At in tellus integer feugiat scelerisque varius morbi. Elit ullamcorper dignissim cras tincidunt. Mattis molestie a iaculis at erat pellentesque adipiscing commodo. Sagittis id consectetur purus ut. Donec ac odio tempor orci dapibus ultrices in. Maecenas volutpat blandit aliquam etiam erat velit scelerisque. Sed augue lacus viverra vitae congue eu.</p>\r\n\r\n<p>Donec enim diam vulputate ut pharetra sit amet aliquam id. Nec nam aliquam sem et tortor consequat id. Arcu cursus vitae congue mauris rhoncus aenean vel. Tortor pretium viverra suspendisse potenti nullam ac tortor. Ullamcorper malesuada proin libero nunc consequat. Volutpat commodo sed egestas egestas fringilla phasellus. Integer malesuada nunc vel risus. Cras adipiscing enim eu turpis egestas pretium aenean pharetra. Orci eu lobortis elementum nibh tellus molestie nunc non blandit. Vel pretium lectus quam id leo in vitae turpis massa.</p>\r\n\r\n<p>Ac tortor dignissim convallis aenean. Neque sodales ut etiam sit amet nisl. A iaculis at erat pellentesque adipiscing commodo elit at. Sed adipiscing diam donec adipiscing tristique. Accumsan sit amet nulla facilisi morbi tempus. Quis viverra nibh cras pulvinar. In vitae turpis massa sed elementum tempus egestas. Eu feugiat pretium nibh ipsum. Egestas congue quisque egestas diam in arcu cursus. Dolor sit amet consectetur adipiscing elit ut aliquam purus sit. Lorem donec massa sapien faucibus et. Mauris commodo quis imperdiet massa tincidunt nunc. Tincidunt vitae semper quis lectus nulla at volutpat diam. Egestas dui id ornare arcu odio ut sem nulla pharetra.</p>\r\n', '2018-10-14', '3.jpg'),
(5, 'habitasse-platea-dictumst-quisque-sagittis-purus-sit-amet-volutpat-consequat', 'habitasse platea dictumst quisque sagittis purus sit amet volutpat consequat', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dui accumsan sit amet nulla facilisi morbi tempus iaculis urna. Tristique senectus et netus et malesuada fames ac turpis. Augue neque gravida in fermentum. Potenti nullam ac tortor vitae purus. Amet commodo nulla facilisi nullam vehicula. In cursus turpis massa tincidunt dui ut ornare lectus. Aliquam malesuada bibendum arcu vitae. Eu turpis egestas pretium aenean pharetra magna ac placerat. Aliquam eleifend mi in nulla. Morbi tempus iaculis urna id volutpat lacus. At volutpat diam ut venenatis tellus in metus vulputate eu. Porta non pulvinar neque laoreet suspendisse interdum consectetur libero. Dignissim sodales ut eu sem integer vitae justo eget. Tellus orci ac auctor augue mauris augue neque gravida. Amet volutpat consequat mauris nunc congue nisi vitae suscipit.</p>\r\n\r\n<p>Auctor eu augue ut lectus. In vitae turpis massa sed elementum tempus. Aliquam purus sit amet luctus venenatis lectus magna fringilla urna. Semper risus in hendrerit gravida rutrum quisque. Ultricies leo integer malesuada nunc vel. Urna id volutpat lacus laoreet non curabitur gravida arcu. Nam libero justo laoreet sit amet cursus sit amet dictum. Tellus molestie nunc non blandit massa enim nec dui. Dui nunc mattis enim ut tellus elementum sagittis vitae. Dictum sit amet justo donec enim. Purus faucibus ornare suspendisse sed. Placerat duis ultricies lacus sed. Enim sit amet venenatis urna. Habitasse platea dictumst vestibulum rhoncus est. Non enim praesent elementum facilisis leo. Feugiat in fermentum posuere urna nec. At urna condimentum mattis pellentesque id nibh.</p>\r\n\r\n<p>Adipiscing elit duis tristique sollicitudin nibh sit. Nibh tortor id aliquet lectus proin nibh. Tellus in hac habitasse platea dictumst vestibulum rhoncus. Tortor vitae purus faucibus ornare suspendisse sed nisi. Nunc mi ipsum faucibus vitae aliquet nec ullamcorper sit amet. At quis risus sed vulputate odio ut enim. Duis at consectetur lorem donec massa. Purus ut faucibus pulvinar elementum integer enim neque. Ultricies tristique nulla aliquet enim tortor at. Nam libero justo laoreet sit amet cursus sit amet. Orci a scelerisque purus semper eget duis. Curabitur vitae nunc sed velit dignissim sodales ut eu. Commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit. Volutpat ac tincidunt vitae semper quis lectus nulla at volutpat. Id venenatis a condimentum vitae sapien pellentesque habitant. Euismod in pellentesque massa placerat duis ultricies. Velit dignissim sodales ut eu sem integer. Senectus et netus et malesuada fames ac turpis egestas. Egestas sed sed risus pretium quam.</p>\r\n\r\n<p>Tempus urna et pharetra pharetra massa massa. Mi bibendum neque egestas congue quisque egestas diam in arcu. Pretium nibh ipsum consequat nisl. Vestibulum rhoncus est pellentesque elit ullamcorper dignissim. Tristique senectus et netus et. Elit ut aliquam purus sit amet luctus venenatis lectus magna. Aliquet bibendum enim facilisis gravida neque convallis. Dolor magna eget est lorem ipsum dolor sit. Varius morbi enim nunc faucibus. Euismod elementum nisi quis eleifend quam adipiscing vitae proin. In nisl nisi scelerisque eu ultrices vitae auctor. Commodo elit at imperdiet dui accumsan. Consectetur adipiscing elit pellentesque habitant morbi. Sed ullamcorper morbi tincidunt ornare. Tempor id eu nisl nunc. Varius sit amet mattis vulputate enim. Felis eget velit aliquet sagittis id consectetur purus ut. In nisl nisi scelerisque eu.</p>\r\n\r\n<p>Ullamcorper velit sed ullamcorper morbi. Non nisi est sit amet facilisis magna etiam tempor orci. Tellus mauris a diam maecenas sed enim. Arcu dictum varius duis at consectetur lorem donec massa. Ullamcorper velit sed ullamcorper morbi tincidunt. Blandit massa enim nec dui nunc mattis. Odio eu feugiat pretium nibh ipsum consequat nisl vel. Aliquam etiam erat velit scelerisque in dictum non consectetur a. Congue quisque egestas diam in arcu cursus. Arcu non sodales neque sodales ut etiam sit. In metus vulputate eu scelerisque felis imperdiet. In ante metus dictum at tempor. Sed arcu non odio euismod lacinia at quis risus.</p>\r\n\r\n<p>Dui faucibus in ornare quam viverra. Est ultricies integer quis auctor elit sed vulputate mi. Amet risus nullam eget felis. Lacinia at quis risus sed. Dignissim enim sit amet venenatis urna cursus eget. Quis eleifend quam adipiscing vitae proin. Orci ac auctor augue mauris augue neque gravida in. Amet luctus venenatis lectus magna fringilla. Molestie a iaculis at erat pellentesque adipiscing commodo. Sem nulla pharetra diam sit amet nisl suscipit adipiscing. Sollicitudin aliquam ultrices sagittis orci a scelerisque purus semper eget. Nisi porta lorem mollis aliquam ut porttitor leo.</p>\r\n\r\n<p>Facilisi morbi tempus iaculis urna. Tempor orci eu lobortis elementum nibh. Mauris pharetra et ultrices neque ornare aenean euismod elementum. Eu non diam phasellus vestibulum lorem sed risus ultricies tristique. Mattis nunc sed blandit libero volutpat sed cras. Elementum eu facilisis sed odio morbi quis commodo. Non curabitur gravida arcu ac tortor dignissim convallis aenean. Elit ut aliquam purus sit. At tellus at urna condimentum. Pellentesque sit amet porttitor eget dolor morbi non. Sed odio morbi quis commodo odio. Adipiscing vitae proin sagittis nisl rhoncus. Aliquam purus sit amet luctus venenatis lectus magna. Mauris in aliquam sem fringilla ut morbi tincidunt augue interdum. Sem integer vitae justo eget magna fermentum.</p>\r\n\r\n<p>Nullam ac tortor vitae purus. Et malesuada fames ac turpis egestas. Arcu dictum varius duis at consectetur lorem. Nullam non nisi est sit amet facilisis magna etiam tempor. Sapien eget mi proin sed. Eget aliquet nibh praesent tristique magna sit amet purus gravida. Eget aliquet nibh praesent tristique magna. Dui id ornare arcu odio ut. Non odio euismod lacinia at quis risus. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet. Iaculis at erat pellentesque adipiscing commodo elit. Sit amet est placerat in egestas. Pulvinar elementum integer enim neque volutpat ac tincidunt vitae. Ac tortor vitae purus faucibus ornare. Posuere morbi leo urna molestie. Eu augue ut lectus arcu bibendum at. Ut enim blandit volutpat maecenas volutpat blandit. Scelerisque varius morbi enim nunc faucibus a pellentesque sit. Pharetra convallis posuere morbi leo urna molestie at elementum. Diam vel quam elementum pulvinar etiam non.</p>\r\n\r\n<p>Eu sem integer vitae justo eget magna fermentum iaculis. Feugiat pretium nibh ipsum consequat nisl vel. Lectus mauris ultrices eros in cursus turpis massa tincidunt dui. In tellus integer feugiat scelerisque varius morbi. Nunc pulvinar sapien et ligula ullamcorper malesuada. Consectetur adipiscing elit ut aliquam purus sit. Nibh sit amet commodo nulla facilisi nullam vehicula ipsum. Cras adipiscing enim eu turpis egestas pretium. Neque egestas congue quisque egestas. Dolor sed viverra ipsum nunc aliquet. Fusce id velit ut tortor. Massa tempor nec feugiat nisl. Ac tincidunt vitae semper quis lectus nulla at. Lorem ipsum dolor sit amet. Aenean sed adipiscing diam donec adipiscing tristique risus nec feugiat. Porta nibh venenatis cras sed felis. Congue quisque egestas diam in arcu cursus. Adipiscing elit pellentesque habitant morbi tristique.</p>\r\n\r\n<p>Aliquam id diam maecenas ultricies. Id faucibus nisl tincidunt eget nullam. Odio tempor orci dapibus ultrices. Tellus elementum sagittis vitae et leo. Tristique senectus et netus et malesuada fames. Tortor pretium viverra suspendisse potenti nullam ac tortor. Tellus in metus vulputate eu scelerisque. Sollicitudin aliquam ultrices sagittis orci a. Metus vulputate eu scelerisque felis imperdiet. Turpis egestas maecenas pharetra convallis posuere morbi leo. Tristique sollicitudin nibh sit amet commodo nulla facilisi nullam. Ac odio tempor orci dapibus ultrices in iaculis. Consectetur adipiscing elit ut aliquam. Porttitor leo a diam sollicitudin tempor id eu.</p>\r\n', '2018-10-14', '4.jpg'),
(6, 'volutpat-sed-cras-ornare-arcu-dui-vivamus-arcu-felis-bibendum', 'volutpat sed cras ornare arcu dui vivamus arcu felis bibendum', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vulputate sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum. Massa tempor nec feugiat nisl pretium fusce. Turpis tincidunt id aliquet risus feugiat in ante metus. Nunc non blandit massa enim nec dui nunc. Odio facilisis mauris sit amet massa vitae tortor condimentum lacinia. Ornare aenean euismod elementum nisi quis eleifend quam adipiscing. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Vel quam elementum pulvinar etiam non. Massa tempor nec feugiat nisl pretium. Id interdum velit laoreet id donec ultrices. Pharetra sit amet aliquam id. Egestas maecenas pharetra convallis posuere morbi leo urna molestie at. Neque viverra justo nec ultrices. Mi quis hendrerit dolor magna eget. Facilisis mauris sit amet massa vitae tortor. Id neque aliquam vestibulum morbi blandit cursus risus at.</p>\r\n\r\n<p>Cursus sit amet dictum sit amet justo donec. Aliquam sem et tortor consequat id porta nibh venenatis cras. Blandit massa enim nec dui nunc mattis. Maecenas pharetra convallis posuere morbi leo urna molestie at. Condimentum vitae sapien pellentesque habitant. Quis lectus nulla at volutpat. Mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus. Dolor magna eget est lorem ipsum dolor. Habitasse platea dictumst quisque sagittis. Vel pretium lectus quam id leo in vitae turpis massa. Cras ornare arcu dui vivamus arcu. At ultrices mi tempus imperdiet nulla malesuada. In hac habitasse platea dictumst. Accumsan lacus vel facilisis volutpat est velit. Facilisis mauris sit amet massa. Sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula ipsum. Nunc sed augue lacus viverra vitae congue. Sit amet facilisis magna etiam. Sit amet consectetur adipiscing elit ut aliquam purus sit amet.</p>\r\n\r\n<p>Viverra justo nec ultrices dui sapien eget mi proin. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. Tellus mauris a diam maecenas sed enim ut sem. Tellus mauris a diam maecenas sed. Vitae suscipit tellus mauris a diam maecenas sed. Morbi quis commodo odio aenean sed adipiscing diam donec. In fermentum posuere urna nec tincidunt praesent. Ipsum nunc aliquet bibendum enim. Nibh venenatis cras sed felis eget velit. Commodo viverra maecenas accumsan lacus vel facilisis. Facilisis magna etiam tempor orci eu lobortis elementum.</p>\r\n\r\n<p>Cras tincidunt lobortis feugiat vivamus at augue eget arcu dictum. Turpis tincidunt id aliquet risus feugiat in ante. Pellentesque diam volutpat commodo sed egestas egestas. Malesuada fames ac turpis egestas maecenas. Enim neque volutpat ac tincidunt vitae semper quis. Sit amet mauris commodo quis imperdiet. Bibendum at varius vel pharetra vel turpis nunc eget lorem. Purus non enim praesent elementum facilisis leo vel fringilla. Egestas tellus rutrum tellus pellentesque eu tincidunt. In aliquam sem fringilla ut morbi tincidunt augue interdum velit. Sit amet mauris commodo quis imperdiet.</p>\r\n\r\n<p>Vel turpis nunc eget lorem dolor. Id semper risus in hendrerit gravida rutrum quisque. Ac turpis egestas maecenas pharetra. Imperdiet sed euismod nisi porta lorem mollis aliquam ut porttitor. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Feugiat in fermentum posuere urna nec tincidunt praesent semper. Sed odio morbi quis commodo odio aenean sed adipiscing. Sit amet nisl purus in mollis nunc sed. Eget mi proin sed libero enim sed faucibus turpis. Quis commodo odio aenean sed adipiscing diam donec adipiscing. Quam pellentesque nec nam aliquam sem et tortor consequat id.</p>\r\n\r\n<p>Et ultrices neque ornare aenean euismod. Nulla pharetra diam sit amet nisl suscipit adipiscing bibendum est. Euismod nisi porta lorem mollis. Ullamcorper morbi tincidunt ornare massa. Enim ut sem viverra aliquet eget sit amet tellus. Nibh venenatis cras sed felis eget. Cras ornare arcu dui vivamus arcu felis bibendum ut tristique. Quam id leo in vitae turpis massa sed elementum tempus. Nisi porta lorem mollis aliquam ut porttitor leo. Pulvinar neque laoreet suspendisse interdum. Et odio pellentesque diam volutpat commodo sed egestas egestas.</p>\r\n\r\n<p>Ut consequat semper viverra nam libero justo. Senectus et netus et malesuada fames ac. Consequat interdum varius sit amet mattis vulputate enim nulla. Pellentesque dignissim enim sit amet venenatis urna cursus eget nunc. At ultrices mi tempus imperdiet nulla malesuada. Ipsum dolor sit amet consectetur. Vitae congue mauris rhoncus aenean vel elit scelerisque. Ultricies mi quis hendrerit dolor magna eget. Nisl suscipit adipiscing bibendum est. Nisi porta lorem mollis aliquam ut. Sit amet volutpat consequat mauris nunc. Ultricies tristique nulla aliquet enim tortor at auctor. Quis hendrerit dolor magna eget. Vitae congue eu consequat ac felis donec et odio pellentesque. Lacus vel facilisis volutpat est.</p>\r\n\r\n<p>Suscipit adipiscing bibendum est ultricies integer quis auctor elit sed. Massa massa ultricies mi quis hendrerit dolor magna eget. Id velit ut tortor pretium viverra suspendisse potenti nullam ac. Pharetra diam sit amet nisl suscipit adipiscing bibendum est. Nisl purus in mollis nunc sed id. Vitae nunc sed velit dignissim. Eu lobortis elementum nibh tellus molestie. Leo integer malesuada nunc vel risus commodo viverra maecenas. Orci phasellus egestas tellus rutrum. At risus viverra adipiscing at in tellus. Nulla facilisi cras fermentum odio eu feugiat. Non quam lacus suspendisse faucibus. Tortor dignissim convallis aenean et tortor. At erat pellentesque adipiscing commodo elit.</p>\r\n\r\n<p>Magna ac placerat vestibulum lectus mauris ultrices eros. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus. Neque egestas congue quisque egestas diam in arcu cursus. Proin sed libero enim sed. Vitae tortor condimentum lacinia quis vel eros. Senectus et netus et malesuada fames ac turpis. Integer enim neque volutpat ac tincidunt vitae semper. Proin fermentum leo vel orci porta non pulvinar. Imperdiet nulla malesuada pellentesque elit eget. Nulla aliquet enim tortor at auctor urna nunc id. Euismod elementum nisi quis eleifend quam adipiscing vitae proin. Nibh cras pulvinar mattis nunc. Nisi porta lorem mollis aliquam. In iaculis nunc sed augue.</p>\r\n\r\n<p>Aliquam sem fringilla ut morbi tincidunt. Sit amet aliquam id diam maecenas ultricies mi eget mauris. Magna fringilla urna porttitor rhoncus dolor. Augue ut lectus arcu bibendum at varius vel. Non diam phasellus vestibulum lorem sed risus. Nullam ac tortor vitae purus faucibus ornare suspendisse sed. Egestas integer eget aliquet nibh praesent tristique magna. Enim nunc faucibus a pellentesque sit amet porttitor eget. Ridiculus mus mauris vitae ultricies. Facilisi nullam vehicula ipsum a arcu cursus vitae. Convallis posuere morbi leo urna molestie at elementum. Viverra nam libero justo laoreet sit amet. Dapibus ultrices in iaculis nunc sed augue. Egestas pretium aenean pharetra magna ac placerat vestibulum lectus mauris. Nec tincidunt praesent semper feugiat nibh sed pulvinar. Non diam phasellus vestibulum lorem sed. Consequat mauris nunc congue nisi vitae suscipit tellus mauris a. Purus in massa tempor nec feugiat nisl pretium fusce. Eros donec ac odio tempor orci dapibus ultrices in. Posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus semper.</p>\r\n', '2018-10-14', '5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_contact`
--

CREATE TABLE `t_contact` (
  `id_contact` int(30) NOT NULL,
  `name` varchar(100) NOT NULL,
  `from` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `date_of_letter` date NOT NULL,
  `mark` enum('0','1') NOT NULL,
  `status` enum('unread','read','delete') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_contact`
--

INSERT INTO `t_contact` (`id_contact`, `name`, `from`, `subject`, `content`, `date_of_letter`, `mark`, `status`) VALUES
(1, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '0', 'delete'),
(3, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '0', 'read'),
(5, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '0', 'read'),
(6, 'Rhandy Rossali', 'rhandyrossali@gmail.com', 'Berita Acara', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', '0', 'delete'),
(7, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '0', 'unread'),
(8, 'Rhandy Rossali', 'rhandyrossali@gmail.com', 'Berita Acara', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', '0', 'delete'),
(9, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '0', 'unread'),
(10, 'Rhandy Rossali', 'rhandyrossali@gmail.com', 'Berita Acara', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', '0', 'unread'),
(11, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '0', 'unread'),
(12, 'Rhandy Rossali', 'rhandyrossali@gmail.com', 'Berita Acara', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', '1', 'delete'),
(13, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '0', 'read'),
(14, 'Rhandy Rossali', 'rhandyrossali@gmail.com', 'Berita Acara', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', '0', 'unread'),
(15, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '0', 'unread'),
(16, 'Rhandy Rossali', 'rhandyrossali@gmail.com', 'Berita Acara', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', '0', 'unread'),
(17, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '0', 'unread'),
(18, 'Rhandy Rossali', 'rhandyrossali@gmail.com', 'Berita Acara', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', '1', 'unread'),
(19, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '0', 'unread'),
(20, 'Rhandy Rossali', 'rhandyrossali@gmail.com', 'Berita Acara', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', '1', 'unread'),
(21, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '1', 'unread'),
(22, 'Rhandy Rossali', 'rhandyrossali@gmail.com', 'Berita Acara', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', '0', 'unread'),
(23, 'Muhammad Farhan Ibrahim', 'muhfarhann.ibrahim@gmail.com', 'Pesan Singkat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-12', '0', 'unread'),
(24, 'Rhandy Rossali', 'rhandyrossali@gmail.com', 'Berita Acara', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', '0', 'unread'),
(28, 'Nama Testing', 'Email.test@gmail.com', 'Test Pesan', 'Hallo, ini test pesan', '2019-02-07', '0', 'unread');

-- --------------------------------------------------------

--
-- Table structure for table `t_gallery`
--

CREATE TABLE `t_gallery` (
  `id_gallery` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `file` varchar(100) NOT NULL,
  `date_upload` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_gallery`
--

INSERT INTO `t_gallery` (`id_gallery`, `title`, `file`, `date_upload`) VALUES
(1, 'Lorem ipsum dolor sit amet 1', 'WhatsApp_Image_2018-11-26_at_13_58_111.jpeg', '2018-11-27'),
(2, 'Lorem ipsum dolor sit amet 2', 'WhatsApp_Image_2018-11-26_at_13_57_54.jpeg', '2018-11-27'),
(3, 'Lorem ipsum dolor sit amet 3', 'WhatsApp_Image_2018-11-26_at_13_58_06(1).jpeg', '2018-11-27'),
(6, 'Lorem ipsum dolor sit amet 4', 'WhatsApp_Image_2018-11-26_at_13_58_11.jpeg', '2018-11-27'),
(7, 'Lorem ipsum dolor sit amet 5', 'WhatsApp_Image_2018-11-26_at_13_58_07.jpeg', '2018-11-28'),
(8, 'Lorem ipsum dolor sit amet 6', 'WhatsApp_Image_2018-11-26_at_13_58_08(1).jpeg', '2018-11-28'),
(9, 'Lorem ipsum dolor sit amet 7', 'WhatsApp_Image_2018-11-26_at_13_58_10(1).jpeg', '2018-11-28'),
(10, 'Lorem ipsum dolor sit amet 8', 'WhatsApp_Image_2018-11-26_at_13_58_10.jpeg', '2018-11-28');

-- --------------------------------------------------------

--
-- Table structure for table `t_jns_pekerjaan`
--

CREATE TABLE `t_jns_pekerjaan` (
  `id_pekerjaan` int(30) NOT NULL,
  `nm_pekerjaan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jns_pekerjaan`
--

INSERT INTO `t_jns_pekerjaan` (`id_pekerjaan`, `nm_pekerjaan`) VALUES
(1, 'Sopir Pribadi'),
(2, 'Tukang Kebun'),
(3, 'Suster Bayi'),
(4, 'Suster Balita'),
(5, 'Asisten Rumah Tangga');

-- --------------------------------------------------------

--
-- Table structure for table `t_login_log`
--

CREATE TABLE `t_login_log` (
  `id_log` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_login_log`
--

INSERT INTO `t_login_log` (`id_log`, `username`, `login_time`, `ip_address`) VALUES
(1, 'farhaanibrahim', '2018-11-10 05:21:23', '::1'),
(2, 'admin', '2018-11-20 22:14:51', '::1'),
(3, 'admin', '2018-11-27 03:05:47', '::1'),
(4, 'admin', '2018-11-27 11:27:38', '::1'),
(5, 'admin', '2018-11-28 05:43:53', '::1'),
(6, 'admin', '2019-01-04 18:17:41', '::1'),
(7, 'admin', '2019-01-04 19:29:40', '::1'),
(8, 'admin', '2019-01-05 05:11:02', '::1'),
(9, 'admin', '2019-01-05 05:19:29', '::1'),
(10, 'admin', '2019-02-06 21:56:18', '::1'),
(11, 'farhaanibrahim', '2019-02-06 21:56:45', '::1'),
(12, 'farhaanibrahim', '2019-02-06 22:32:00', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `t_order`
--

CREATE TABLE `t_order` (
  `id_order` int(30) NOT NULL,
  `id_worker` int(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `email` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `date_of_order` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_order`
--

INSERT INTO `t_order` (`id_order`, `id_worker`, `nama`, `alamat`, `no_hp`, `email`, `keterangan`, `date_of_order`) VALUES
(1, 2, 'Muhammad Fakhri Ismail', 'Bogor, Cimanggu Pahlawan, Jl. Pahlawan No.5 RT 01/02, Kec. Tanah Sareal, Kel. Kedung Jaya, Bogor.', '081287658267', 'fakhri_ismail@yahoo.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13'),
(2, 3, 'Fakira Ramadhani Azzahra', 'Bogor, Cimanggu Pahlawan, Jl. Pahlawan No.5 RT 01/02, Kec. Tanah Sareal, Kel. Kedung Jaya, Bogor.', '081265798677', 'rara@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13'),
(3, 6, 'Abisonna', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cursus mattis molestie a iaculis at erat pellentesque. Maecenas accumsan lacus vel facilisis volutpat est. Nullam eget felis eget nunc lobortis mattis aliquam. Cras tincidunt lobortis feugiat vivamus at. Massa eget egestas purus viverra accumsan in. Est ullamcorper eget nulla facilisi etiam dignissim diam. Quisque egestas diam in arcu cursus. Tristique senectus et netus et malesuada. Nunc congue nisi vitae suscipit tellus mauris. Vel facilisis volutpat est velit. ', '085765431287', 'abisonna@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cursus mattis molestie a iaculis at erat pellentesque. Maecenas accumsan lacus vel facilisis volutpat est. Nullam eget felis eget nunc lobortis mattis aliquam. Cras tincidunt lobortis feugiat vivamus at. Massa eget egestas purus viverra accumsan in. Est ullamcorper eget nulla facilisi etiam dignissim diam. Quisque egestas diam in arcu cursus. Tristique senectus et netus et malesuada. Nunc congue nisi vitae suscipit tellus mauris. Vel facilisis volutpat est velit. ', '2018-10-25'),
(4, 5, 'Nama test', 'Alamat Test', '081314118157', 'Email.test@gmail.com', 'Keterangan Test', '2019-02-07');

-- --------------------------------------------------------

--
-- Table structure for table `t_recruitment`
--

CREATE TABLE `t_recruitment` (
  `id_rec` int(30) NOT NULL,
  `nm_lengkap` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jns_klmn` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `no_hp_ortu1` varchar(12) NOT NULL,
  `no_hp_ortu2` varchar(12) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tgl` date NOT NULL,
  `lampiran` varchar(100) NOT NULL,
  `foto_ktp` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_recruitment`
--

INSERT INTO `t_recruitment` (`id_rec`, `nm_lengkap`, `tgl_lahir`, `jns_klmn`, `alamat`, `no_hp`, `no_hp_ortu1`, `no_hp_ortu2`, `email`, `tgl`, `lampiran`, `foto_ktp`) VALUES
(1, 'Muhammad Farhan Ibrahim', '1997-07-26', 'Laki - laki', 'Cimanggu Pahlawan no.5 RT 01/02, Jl. Tentara Pelajar Kel. Kedung Jaya, Kec. Tanah Sareal Kota bogor.', '081314118157', '08121104908', '-', 'farhaaanibrahim@gmail.com', '2018-10-10', 'awdwad.pdf', ''),
(2, 'Rhandy Rossali', '1996-10-19', 'Laki - laki', 'Bekasi', '081314118156', '081267644567', '081276543947', 'rhandyrossali@gmmail.com', '2018-10-12', '', ''),
(6, 'Muhammad Farhan Ibrahim', '1997-07-26', 'Laki - laki', 'Cimanggu Pahlawan no.5 RT 01/02, Jl. Tentara Pelajar Kel. Kedung Jaya, Kec. Tanah Sareal Kota bogor.', '081314118157', '08121104908', '-', 'farhaaanibrahim@gmail.com', '2018-10-10', '', ''),
(7, 'Rhandy Rossali', '1996-10-19', 'Laki - laki', 'Bekasi', '081314118156', '081267644567', '081276543947', 'rhandyrossali@gmmail.com', '2018-10-12', '', ''),
(8, 'Muhammad Farhan Ibrahim', '1997-07-26', 'Laki - laki', 'Cimanggu Pahlawan no.5 RT 01/02, Jl. Tentara Pelajar Kel. Kedung Jaya, Kec. Tanah Sareal Kota bogor.', '081314118157', '08121104908', '-', 'farhaaanibrahim@gmail.com', '2018-10-10', '', ''),
(9, 'Rhandy Rossali', '1996-10-19', 'Laki - laki', 'Bekasi', '081314118156', '081267644567', '081276543947', 'rhandyrossali@gmmail.com', '2018-10-12', '', ''),
(10, 'Muhammad Farhan Ibrahim', '1997-07-26', 'Laki - laki', 'Cimanggu Pahlawan no.5 RT 01/02, Jl. Tentara Pelajar Kel. Kedung Jaya, Kec. Tanah Sareal Kota bogor.', '081314118157', '08121104908', '-', 'farhaaanibrahim@gmail.com', '2018-10-10', '', ''),
(11, 'Rhandy Rossali', '1996-10-19', 'Laki - laki', 'Bekasi', '081314118156', '081267644567', '081276543947', 'rhandyrossali@gmmail.com', '2018-10-12', '', ''),
(12, 'Muhammad Farhan Ibrahim', '1997-07-26', 'Laki - laki', 'Cimanggu Pahlawan no.5 RT 01/02, Jl. Tentara Pelajar Kel. Kedung Jaya, Kec. Tanah Sareal Kota bogor.', '081314118157', '08121104908', '-', 'farhaaanibrahim@gmail.com', '2018-10-10', '', ''),
(13, 'Rhandy Rossali', '1996-10-19', 'Laki - laki', 'Bekasi', '081314118156', '081267644567', '081276543947', 'rhandyrossali@gmmail.com', '2018-10-12', '', ''),
(14, 'Muhammad Farhan Ibrahim', '1997-07-26', 'Laki - laki', 'Cimanggu Pahlawan no.5 RT 01/02, Jl. Tentara Pelajar Kel. Kedung Jaya, Kec. Tanah Sareal Kota bogor.', '081314118157', '08121104908', '-', 'farhaaanibrahim@gmail.com', '2018-10-10', '', ''),
(15, 'Rhandy Rossali', '1996-10-19', 'Laki - laki', 'Bekasi', '081314118156', '081267644567', '081276543947', 'rhandyrossali@gmmail.com', '2018-10-12', '', ''),
(16, 'Muhammad Farhan Ibrahim', '1997-07-26', 'Laki - laki', 'Cimanggu Pahlawan no.5 RT 01/02, Jl. Tentara Pelajar Kel. Kedung Jaya, Kec. Tanah Sareal Kota bogor.', '081314118157', '08121104908', '-', 'farhaaanibrahim@gmail.com', '2018-10-10', '', ''),
(17, 'Rhandy Rossali', '1996-10-19', 'Laki - laki', 'Bekasi', '081314118156', '081267644567', '081276543947', 'rhandyrossali@gmmail.com', '2018-10-12', '', ''),
(18, 'Muhammad Farhan Ibrahim', '1997-07-26', 'Laki - laki', 'Cimanggu Pahlawan no.5 RT 01/02, Jl. Tentara Pelajar Kel. Kedung Jaya, Kec. Tanah Sareal Kota bogor.', '081314118157', '08121104908', '-', 'farhaaanibrahim@gmail.com', '2018-10-10', '', ''),
(19, 'Rhandy Rossali', '1996-10-19', 'Laki - laki', 'Bekasi', '081314118156', '081267644567', '081276543947', 'rhandyrossali@gmmail.com', '2018-10-12', '', ''),
(20, 'Muhammad Farhan Ibrahim', '1997-07-26', 'Laki - laki', 'Cimanggu Pahlawan no.5 RT 01/02, Jl. Tentara Pelajar Kel. Kedung Jaya, Kec. Tanah Sareal Kota bogor.', '081314118157', '08121104908', '-', 'farhaaanibrahim@gmail.com', '2018-10-10', '', ''),
(21, 'Rhandy Rossali', '1996-10-19', 'Laki - laki', 'Bekasi', '081314118156', '081267644567', '081276543947', 'rhandyrossali@gmmail.com', '2018-10-12', '', ''),
(22, 'Muhammad Farhan Ibrahim', '1997-07-26', 'Laki - laki', 'Cimanggu Pahlawan no.5 RT 01/02, Jl. Tentara Pelajar Kel. Kedung Jaya, Kec. Tanah Sareal Kota bogor.', '081314118157', '08121104908', '-', 'farhaaanibrahim@gmail.com', '2018-10-10', '', ''),
(25, 'Bagus Panghegar', '1999-10-10', 'L', 'Street\r\n1362  Holden Street\r\nCity\r\nLAKEVILLE\r\nState\r\nPA\r\nState Full\r\nPennsylvania\r\nZip Code\r\n18438\r\nPhone Number\r\n619-279-0921\r\nMobile Number\r\n412-370-2864\r\n', '085765431287', '081287650947', '', 'baguspanghegar@gmail.com', '2018-10-25', '', ''),
(26, 'aaa', '2018-11-15', 'L', 'bekasi', '0808', '8787', '78787', 'rossarandy@gmaiil.com', '2018-11-30', '', ''),
(27, 'Fakhri', '1997-01-09', 'L', 'Bogor Cimanggu', '0813131', '081314118157', '081314118157', 'fakhri@gmail.com', '2019-01-04', '', ''),
(28, 'Fakhira Ramadhani Azzahra', '2010-10-10', 'Wanita', 'Cimanggu Pahlawan no.5 Bogor', '081314118157', '08121104908', '08111119300', 'fakhira@gmail.com', '2019-02-03', 'mac-os-sierra-color-splash-purple-do-2560x1440.jpg', 'Wall-off-white.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_sent`
--

CREATE TABLE `t_sent` (
  `id_message` int(30) NOT NULL,
  `name` varchar(100) NOT NULL,
  `to` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `date_of_letter` date NOT NULL,
  `status` enum('sent','draft') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_sent`
--

INSERT INTO `t_sent` (`id_message`, `name`, `to`, `subject`, `content`, `date_of_letter`, `status`) VALUES
(1, 'Muhammad Farhan Ibrahim', 'rhandyrossali@gmail.com', 'Reply Message', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', 'sent'),
(2, 'Rhandy Rossali', 'farhaaanibrahim@gmail.com', 'Reply Message', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', 'sent'),
(3, 'Muhammad Farhan Ibrahim', 'rhandyrossali@gmail.com', 'Reply Message', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', 'sent'),
(4, 'Rhandy Rossali', 'farhaaanibrahim@gmail.com', 'Reply Message', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', 'sent'),
(5, 'Muhammad Farhan Ibrahim', 'rhandyrossali@gmail.com', 'Reply Message', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', 'draft'),
(6, 'Rhandy Rossali', 'farhaaanibrahim@gmail.com', 'Reply Message', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', 'draft'),
(7, 'Muhammad Farhan Ibrahim', 'rhandyrossali@gmail.com', 'Reply Message', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor purus non enim praesent. Condimentum vitae sapien pellentesque habitant morbi. Nunc sed id semper risus in hendrerit. Vulputate sapien nec sagittis aliquam. Purus sit amet volutpat consequat mauris nunc. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Sagittis orci a scelerisque purus. Varius duis at consectetur lorem donec massa sapien.', '2018-10-13', 'draft');

-- --------------------------------------------------------

--
-- Table structure for table `t_status`
--

CREATE TABLE `t_status` (
  `id_status` int(30) NOT NULL,
  `nm_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status`
--

INSERT INTO `t_status` (`id_status`, `nm_status`) VALUES
(1, 'Dalam Pelatihan'),
(2, 'Siap Bekerja'),
(3, 'Telah Bekerja'),
(4, 'Telah Dipesan');

-- --------------------------------------------------------

--
-- Table structure for table `t_suku`
--

CREATE TABLE `t_suku` (
  `id_suku` int(10) NOT NULL,
  `nm_suku` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_suku`
--

INSERT INTO `t_suku` (`id_suku`, `nm_suku`) VALUES
(1, 'Bali'),
(2, 'Banjar'),
(3, 'Banten'),
(4, 'Bugis'),
(5, 'Cirebon'),
(6, 'Jawa'),
(7, 'Lampung'),
(8, 'Madura'),
(9, 'Makassar'),
(10, 'Minangkabau'),
(11, 'NTT'),
(12, 'Sasak'),
(13, 'Sulawesi'),
(14, 'Sunda');

-- --------------------------------------------------------

--
-- Table structure for table `t_worker`
--

CREATE TABLE `t_worker` (
  `id_worker` int(30) NOT NULL,
  `nm_lengkap` varchar(50) NOT NULL,
  `nm_panggilan` varchar(50) NOT NULL,
  `jns_kelamin` enum('pria','wanita') NOT NULL,
  `profesi` varchar(20) NOT NULL,
  `tb` varchar(20) NOT NULL,
  `bb` varchar(20) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `pendidikan` varchar(20) NOT NULL,
  `punya_anak` varchar(10) NOT NULL,
  `asal` varchar(20) NOT NULL,
  `suku` varchar(20) NOT NULL,
  `domisili` varchar(20) NOT NULL,
  `keahlian` text NOT NULL,
  `pengalaman` text NOT NULL,
  `umur` int(20) NOT NULL,
  `status` int(11) NOT NULL,
  `status_kawin` varchar(20) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `salary` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_worker`
--

INSERT INTO `t_worker` (`id_worker`, `nm_lengkap`, `nm_panggilan`, `jns_kelamin`, `profesi`, `tb`, `bb`, `agama`, `pendidikan`, `punya_anak`, `asal`, `suku`, `domisili`, `keahlian`, `pengalaman`, `umur`, `status`, `status_kawin`, `foto`, `salary`) VALUES
(2, 'Muhammad Farhan Ibrahim', 'Farhan', 'pria', '3', '160', '55', 'islam', 'sarjana', '0', 'Bogor', '14', 'Bogor', '<ol>\r\n	<li>Skill 1</li>\r\n	<li>Skill 2</li>\r\n	<li>Skill 3</li>\r\n	<li>Skill 4</li>\r\n	<li>Skill 5</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Exp 1</li>\r\n	<li>Exp 2</li>\r\n	<li>Exp 3</li>\r\n	<li>Exp 4</li>\r\n	<li>Exp5</li>\r\n</ol>\r\n', 21, 2, 'single', 'user-blue11.png', 5000000),
(3, 'Feby Melati Maharani', 'Feby', 'wanita', '1', '160', '50', 'islam', 'diploma', '0', 'Indramayu', '14', 'Bogor', '<ol>\r\n	<li>Skill 1</li>\r\n	<li>Skill 2</li>\r\n</ol>\r\n', '<ol>\r\n	<li>exp 1</li>\r\n	<li>exp 2</li>\r\n</ol>\r\n', 21, 1, 'single', 'user-blue1.png', 4000000),
(4, 'Bagas Nur A', 'Bagas', 'pria', '2', '160', '60', 'islam', 'sarjana', '0', 'Jakarta', '3', 'Jakarta', '<ol>\r\n	<li>Skill 1</li>\r\n	<li>Skill 2</li>\r\n	<li>Skill 3</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Exp 1</li>\r\n	<li>Exp 2</li>\r\n	<li>Exp3</li>\r\n</ol>\r\n', 22, 2, 'single', 'user-image-with-black-background_318-34564.jpg', 3500000),
(5, 'Akbar Wibawanto', 'Akbar', 'pria', '2', '160', '60', 'islam', 'sarjana', '1', 'Depok', '6', 'Jakarta', '<ol>\r\n	<li>Skill 1</li>\r\n	<li>Skill 2</li>\r\n	<li>Skill 3</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Exp 1</li>\r\n	<li>Exp 2</li>\r\n	<li>Exp 3</li>\r\n</ol>\r\n', 52, 1, 'married', 'user-blue.png', 3000000),
(6, 'Rhandy Rossali', 'Rhandy', 'pria', '1', '160', '60', 'islam', 'diploma', '0', 'Jakarta', '6', 'Bekasi', '<ol>\r\n	<li>Skill 1</li>\r\n	<li>Skill 2</li>\r\n	<li>Skill 3</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Exp 1</li>\r\n	<li>Exp 2</li>\r\n	<li>Exp 3</li>\r\n</ol>\r\n', 52, 2, 'single', 'male-user-shadow_318-34042.jpg', 4000000),
(7, 'Rinaldo Anugrah W', 'Aldo', 'pria', '2', '160', '60', 'islam', 'diploma', '0', 'Jakarta', '6', 'Tangerang', '<ol>\r\n	<li>Skill 1</li>\r\n	<li>Skill 2</li>\r\n	<li>Skill 3</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Exp 1</li>\r\n	<li>Exp 2</li>\r\n	<li>Exp 3</li>\r\n</ol>\r\n', 27, 1, 'single', 'matt-icons_preferences-desktop-personal.png', 3000000),
(8, 'Bobby Siboro', 'Bobby', 'pria', '2', '160', '55', 'katholik', 'diploma', '2', 'Bogor', '14', 'Bogor', '<ol>\r\n	<li>Skill 1</li>\r\n	<li>Skill 2</li>\r\n	<li>Skill 3</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Exp 1</li>\r\n	<li>Exp 2</li>\r\n	<li>Exp 3</li>\r\n</ol>\r\n', 21, 2, 'married', 'Untitled-1-512.png', 3999999),
(9, 'Bagaskara LP', 'Bagas', 'pria', '3', '170', '70', 'islam', 'sarjana', '1', 'Jakarta', '6', 'Jakarta', '<ol>\r\n	<li>Skill 1</li>\r\n	<li>Skill 2</li>\r\n	<li>Skill 3</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Exp 1</li>\r\n	<li>Exp 2</li>\r\n	<li>Exp 3</li>\r\n</ol>\r\n', 21, 2, 'married', 'max-rehkopf.png', 4000000),
(10, 'Wafi Pandega', 'Johny english', 'pria', '3', '170', '60', 'islam', 'sarjana', '0', 'Bogor', '14', 'Bogor', '<ol>\r\n	<li>Skill 1</li>\r\n	<li>Skill 2</li>\r\n	<li>Skill 3</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Exp 1</li>\r\n	<li>Exp 2</li>\r\n	<li>Exp 3</li>\r\n</ol>\r\n', 35, 2, 'single', '131629.png', 4500000),
(11, 'Ari Irwandi', 'Ari', 'pria', '2', '160', '60', 'islam', 'diploma', '0', 'Garut', '14', 'Depok', '<ol>\r\n	<li>Skill 1</li>\r\n	<li>Skill 2</li>\r\n	<li>Skill 3</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Exp 1</li>\r\n	<li>Exp 2</li>\r\n	<li>Exp 3</li>\r\n</ol>\r\n', 37, 2, 'single', 'personal-user-illustration-@2x.png', 3000000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_admin`
--
ALTER TABLE `t_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_artikel`
--
ALTER TABLE `t_artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `t_contact`
--
ALTER TABLE `t_contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `t_gallery`
--
ALTER TABLE `t_gallery`
  ADD PRIMARY KEY (`id_gallery`);

--
-- Indexes for table `t_jns_pekerjaan`
--
ALTER TABLE `t_jns_pekerjaan`
  ADD PRIMARY KEY (`id_pekerjaan`);

--
-- Indexes for table `t_login_log`
--
ALTER TABLE `t_login_log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `t_order`
--
ALTER TABLE `t_order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `t_recruitment`
--
ALTER TABLE `t_recruitment`
  ADD PRIMARY KEY (`id_rec`);

--
-- Indexes for table `t_sent`
--
ALTER TABLE `t_sent`
  ADD PRIMARY KEY (`id_message`);

--
-- Indexes for table `t_status`
--
ALTER TABLE `t_status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `t_suku`
--
ALTER TABLE `t_suku`
  ADD PRIMARY KEY (`id_suku`);

--
-- Indexes for table `t_worker`
--
ALTER TABLE `t_worker`
  ADD PRIMARY KEY (`id_worker`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_admin`
--
ALTER TABLE `t_admin`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_artikel`
--
ALTER TABLE `t_artikel`
  MODIFY `id_artikel` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_contact`
--
ALTER TABLE `t_contact`
  MODIFY `id_contact` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `t_gallery`
--
ALTER TABLE `t_gallery`
  MODIFY `id_gallery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `t_jns_pekerjaan`
--
ALTER TABLE `t_jns_pekerjaan`
  MODIFY `id_pekerjaan` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_login_log`
--
ALTER TABLE `t_login_log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `t_order`
--
ALTER TABLE `t_order`
  MODIFY `id_order` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_recruitment`
--
ALTER TABLE `t_recruitment`
  MODIFY `id_rec` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `t_sent`
--
ALTER TABLE `t_sent`
  MODIFY `id_message` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_status`
--
ALTER TABLE `t_status`
  MODIFY `id_status` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_suku`
--
ALTER TABLE `t_suku`
  MODIFY `id_suku` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `t_worker`
--
ALTER TABLE `t_worker`
  MODIFY `id_worker` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
